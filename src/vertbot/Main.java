package vertbot;

import java.util.Arrays;
import java.util.HashMap;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import vertbot.commands.Commands;
import vertbot.music.MusicPlayer;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.BotGuildCheck;
import vertbot.util.ColorUtil;
import vertbot.util.GetUsers;
import vertbot.util.Processes;
import vertbot.util.Responses;
import vertbot.util.Shard;
import vertbot.util.Utils;

public class Main extends ListenerAdapter
{
	public static final String DEV_ID = "142782417994907648";

	public static String version = "1.7.2";

	public static boolean dev = false;

	int shardId;

	public static Shard[] shards = new Shard[3];

	public static int playlistSize = 200;

	public static void main(String[] args) throws Exception
	{
		System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

		String token = "";

		if (dev == true)
		{
			// Dev Token
			token = "{token-here}";
		} else
		{
			// Normal Token
			token = "{token-here}";
		}

		for (int i = 0; i < shards.length; i++)
		{
			shards[i] = new Shard(i, shards.length, token);
			Thread.sleep(5500);
		}

		AudioSourceManagers.registerRemoteSources(apm);
	}

	public static Shard[] getShards()
	{
		return shards;
	}

	static HashMap<String, GuildSettings> guildSettingsCache = new HashMap<>();
	public static HashMap<String, MusicPlayer> players = new HashMap<String, MusicPlayer>();
	static Commands commands = new Commands();
	private static MessageChannel channelA, channelB;

	public static MessageChannel getA()
	{
		return channelA;
	}

	public static MessageChannel getB()
	{
		return channelB;
	}

	public static void setA(MessageChannel channel)
	{
		channelA = channel;
	}

	public static void setB(MessageChannel channel)
	{
		channelB = channel;
	}

	public static Commands getCommands()
	{
		return commands;
	}

	/** any persistence in commands will be wiped. */
	public static void reloadCommands()
	{
		commands = new Commands();
	}

	public static GuildSettings getGuildSettings(String id)
	{
		// create settings if they dont exist
		if (!guildSettingsCache.containsKey(id))
		{
			GuildSettings gs = new GuildSettings(id);
			gs.load();

			guildSettingsCache.put(id, gs);
		}

		return guildSettingsCache.get(id);
	}

	@Override
	public void onGuildJoin(GuildJoinEvent event)
	{
		System.out.println("Joined guild: " + event.getGuild().getName() + " (" + event.getGuild().getId() + ") Users: " + GetUsers.Users(event.getGuild()) + " Bots: " + GetUsers.Bots(event.getGuild()) + " | Bot Server Check Return: " + BotGuildCheck.Check(event.getGuild()));

		try
		{
			if(BotGuildCheck.Check(event.getGuild()) == true) {
				TextChannel chan = Utils.getLogicalChannel(event.getGuild());
				chan.sendMessage(event.getGuild().getOwner().getAsMention() + "Your server has been blacklisted by my dev or your server was claimed as a bot server. Sorry about that. If you think this was a mistake please join my server and speak with my dev himself: https://discord.gg/WPUU2dF").complete();
				event.getGuild().leave().complete();
			} else {
				final long serv = Arrays.stream(Main.getShards())
			            .filter(s ->  s != null && s.jda != null)
			            .map(s -> s.jda.getGuilds().size())
			            .reduce(0, (a, b) -> a + b);
				Processes.node("dbshard", "" + serv);
				Responses.join(event);
			}
		} catch(Exception e) {
			return;
		}
	}

	@Override
	public void onGuildLeave(GuildLeaveEvent event)
	{
		System.out.println("Left guild: " + event.getGuild().getName() + " (" + event.getGuild().getId() + ")");

		try
		{
			if(BotGuildCheck.Check(event.getGuild()) == true) {
				TextChannel chan = Utils.getLogicalChannel(event.getGuild());
				chan.sendMessage(event.getGuild().getOwner().getAsMention() + "Your server has been blacklisted by my dev or your server was claimed as a bot server. Sorry about that. If you think this was a mistake please join my server and speak with my dev himself: https://discord.gg/WPUU2dF").complete();
				event.getGuild().leave().complete();
			} else {
				final long serv = Arrays.stream(Main.getShards())
			            .filter(s ->  s != null && s.jda != null)
			            .map(s -> s.jda.getGuilds().size())
			            .reduce(0, (a, b) -> a + b);
				Processes.node("dbshard", "" + serv);
			}
		} catch(Exception e) {
			return;
		}
	}

	@Override
	public void onGuildMemberJoin(GuildMemberJoinEvent event)
	{
		Guild guild = event.getGuild();
		GuildSettings gs = getGuildSettings(guild.getId());

		try
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("User Joined: " + event.getUser().getName());
			ebuilder.setColor(ColorUtil.success());
			ebuilder.setFooter("", null);
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getGuild().getTextChannelById(gs.getJSON().optString("jlog", "")).sendMessage(message).complete();
			if(BotGuildCheck.Check(event.getGuild()) == true) {
				TextChannel chan = Utils.getLogicalChannel(event.getGuild());
				chan.sendMessage(event.getGuild().getOwner().getAsMention() + "Your server has been blacklisted by my dev or your server was claimed as a bot server. Sorry about that. If you think this was a mistake please join my server and speak with my dev himself: https://discord.gg/WPUU2dF").complete();
				event.getGuild().leave().complete();
			}
		} catch (Exception e)
		{
			return;
		}
	}

	@Override
	public void onGuildMemberLeave(GuildMemberLeaveEvent event)
	{
		Guild guild = event.getGuild();
		GuildSettings gs = getGuildSettings(guild.getId());

		try
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("User Left: " + event.getUser().getName());
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getGuild().getTextChannelById(gs.getJSON().optString("jlog", "")).sendMessage(message).complete();
			if(BotGuildCheck.Check(event.getGuild()) == true) {
				TextChannel chan = Utils.getLogicalChannel(event.getGuild());
				chan.sendMessage(event.getGuild().getOwner().getAsMention() + "Your server has been blacklisted by my dev or your server was claimed as a bot server. Sorry about that. If you think this was a mistake please join my server and speak with my dev himself: https://discord.gg/WPUU2dF").complete();
				event.getGuild().leave().complete();
			}
		} catch (Exception e)
		{
			return;
		}
	}

	/** returns the music player associated with the given guild */
	public static MusicPlayer getPlayer(Guild guild)
	{
		String id = guild.getId();

		MusicPlayer player = players.get(id);

		// create player if it doesnt exist
		if (player == null)
		{
			players.put(id, new MusicPlayer(guild));
		}

		player = players.get(id);
		return player;
	}

	public static AudioPlayerManager apm = new DefaultAudioPlayerManager();

	@Override
	public void onMessageReceived(MessageReceivedEvent event)
	{
		try
		{
			if (event.getAuthor().isBot())
			{
				return;
			} // Other bots
		} catch (Exception e)
		{
			System.out.println("Error for some fucking reason...");
		}
		try
		{
			String prefix = "";

			if (dev == true)
			{
				prefix = "[=";
			} else
			{
				prefix = "-";
			}

			Message message = event.getMessage();
			String content = message.getContentRaw();
			Guild guild = event.getGuild();
			GuildSettings gs = getGuildSettings(guild.getId());
			String trigger = gs.getJSON().optString("prefix", prefix);

			Args cmd;

			if (content.startsWith(event.getGuild().getSelfMember().getAsMention()))
			{
				cmd = new Args(new Args(content).rest());// new Args(content, 1,
															// -1);
			} else if (!trigger.equals("") && content.startsWith(trigger))
			{
				cmd = new Args(content.substring(trigger.length()));
			}
			// regular message (other than bot)
			else if (!event.getAuthor().getId().equals(event.getJDA().getSelfUser().getId()))
			{
				if (getA() == null || getB() == null)
				{
					return;
				}

				MessageChannel src = null;
				MessageChannel dest = null;
				String msg = event.getMessage().getContentRaw();

				// check/resolve src/dest channels
				if (event.getChannel().getId().equals(getA().getId()))
				{
					src = getA();
					dest = getB();
				} else if (event.getChannel().getId().equals(getB().getId()))
				{
					src = getB();
					dest = getA();
				}

				// bridge the message
				if (src != null && dest != null && !msg.equals(""))
				{
					dest.sendMessage(event.getAuthor().getName() + ": " + msg).complete();
				}

				return;
			} else
			{
				return;
			}

			new Thread()
			{
				@Override
				public void run()
				{
					commands.doCommand(event, cmd);
				}
			}.start();

			try
			{
				// System.out.println(Utils.debugOut(event, true));
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Command used in " + event.getChannel().getName());
				ebuilder.setDescription(event.getAuthor().getName() + ": " + event.getMessage().getContentDisplay());
				ebuilder.setFooter("Command may not be a valid command.", null);
				ebuilder.setColor(ColorUtil.neutral());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message msg = mbuilder.build();
				event.getGuild().getTextChannelById(gs.getJSON().optString("log", "")).sendMessage(msg).complete();
			} catch (Exception e)
			{
				// System.out.println("Rate-Limit on guild log or other");
				return;
			}
		} catch (Exception e)
		{
			return;
		}
	}
}
