package vertbot.commands;

import java.util.HashMap;

import vertbot.commands.settings.JoinLogCommand;
import vertbot.commands.settings.LogChatCommand;
import vertbot.commands.settings.PrefixCommand;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class SettingsCommand extends Command
{
	public SettingsCommand()
	{
		super();
		
		this.addCommand(new PrefixCommand());
		this.addCommand(new LogChatCommand());
		this.addCommand(new JoinLogCommand());
	}
	
	static HashMap<String, GuildSettings> guildSettingsCache = new HashMap<>();
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.isFromType(ChannelType.PRIVATE))
		{
			Responses.noPrivate(event);
			
			return;
		}
		
		if(args.size() == 0)
		{
			Responses.respond(event, "```" + new HelpCommand().getHelp(event, this) + "```");
			return;
		}
		
		super.doCommand(event, args);
	}
	
	@Override
	public String getName()
	{
		return "settings";
	}
	
	@Override
	public String getDescription()
	{
		return "Change general settings <Sub-menu>";
	}
	
}
