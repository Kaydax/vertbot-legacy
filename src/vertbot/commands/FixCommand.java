package vertbot.commands;

import vertbot.util.Args;
import vertbot.util.ColorUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class FixCommand extends Command
{

	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		EmbedBuilder ebuilder = new EmbedBuilder();
		ebuilder.addField("Join my server for support:", "[Join Now](https://discord.gg/WPUU2dF)", false);
		ebuilder.setColor(ColorUtil.neutral());
		MessageEmbed embed = ebuilder.build();
		MessageBuilder mbuilder = new MessageBuilder();
		mbuilder.setEmbed(embed);
		Message message = mbuilder.build();
		event.getChannel().sendMessage(message).complete();
	}

	@Override
	public String getName()
	{
		return "support";
	}

	@Override
	public String getDescription()
	{
		return "Send you a invite to the support server.";
	}

	@Override
	public String getPermission()
	{
		return "";// Permissions.DEV;
	}
}
