package vertbot.commands.settings;

import vertbot.Main;
import vertbot.commands.Command;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Permissions;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class PrefixCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Guild guild = event.getGuild();
		GuildSettings gs = Main.getGuildSettings(guild.getId());
		
		//setting prefix
		if(args.size() > 0)
		{
			String prefix = args.concat();
			
			gs.getJSON().put("prefix", prefix);
			gs.save();
			
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Prefix set");
			ebuilder.setDescription("Prefix set to `" + prefix + "`");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		//clearing prefix
		else
		{
			gs.getJSON().remove("prefix");
			gs.save();
			
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Prefix reset");
			ebuilder.setDescription("Prefix reset to default.");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "prefix";
	}
	
	@Override
	public String getUsage()
	{
		return "<prefix>";
	}
	
	@Override
	public String getDescription()
	{
		return "Change the chat prefix I respond to";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.ADMIN;
	}
}
