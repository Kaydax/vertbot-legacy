package vertbot.commands.settings;

import vertbot.Main;
import vertbot.commands.Command;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Permissions;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class LogChatCommand extends Command
{

	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Guild guild = event.getGuild();
		GuildSettings gs = Main.getGuildSettings(guild.getId());

		// setting log channel
		if (args.size() > 0)
		{
			try
			{
				if (event.getGuild().getTextChannelById(args.concat(0)).canTalk())
				{
					String channel = args.concat(0);

					String chanName = event.getGuild().getTextChannelById(channel).getName();

					gs.getJSON().put("log", channel);
					gs.save();

					EmbedBuilder ebuilder = new EmbedBuilder();
					ebuilder.setTitle("Success: Log channel set");
					ebuilder.setDescription("Log channel set to `" + chanName + "`");
					ebuilder.setColor(ColorUtil.success());
					MessageEmbed embed = ebuilder.build();
					MessageBuilder mbuilder = new MessageBuilder();
					mbuilder.setEmbed(embed);
					Message message = mbuilder.build();
					event.getChannel().sendMessage(message).complete();
				}
			} catch (Exception e)
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Log channel could not be set");
				ebuilder.setDescription(
						"It seems that the channel you tried to set was invalid, this could be that I cannot"
								+ " speak in that channel or it doesn't exist.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
		} else
		{
			gs.getJSON().remove("log");
			gs.save();

			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Log channel unset");
			ebuilder.setDescription("The log channel was unset");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}

	@Override
	public String getName()
	{
		return "setlog";
	}

	@Override
	public String getUsage()
	{
		return "<channel id>";
	}

	@Override
	public String getDescription()
	{
		return "Set or change what channel I send my logs to.";
	}

	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
}
