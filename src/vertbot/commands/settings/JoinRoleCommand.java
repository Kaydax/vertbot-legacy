package vertbot.commands.settings;

import vertbot.Main;
import vertbot.commands.Command;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Permissions;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class JoinRoleCommand extends Command
{

	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Guild guild = event.getGuild();
		GuildSettings gs = Main.getGuildSettings(guild.getId());

		// setting log channel
		if (args.size() > 0)
		{
			try
			{
				Role role = event.getGuild().getRolesByName(args.concat(0), false).get(0);

				gs.getJSON().put("jrole", role.getId());
				gs.save();

				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Success: Join role set");
				ebuilder.setDescription("Join role set to `" + role.getName() + "`");
				ebuilder.setColor(ColorUtil.success());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			} catch (Exception e)
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Join role could not be set");
				ebuilder.setDescription("It seems that the role you tried to set was invalid, this could be that I cannot find that role.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
		} else
		{
			gs.getJSON().remove("jrole");
			gs.save();

			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Join role unset");
			ebuilder.setDescription("The join role was unset");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}

	@Override
	public String getName()
	{
		return "setjoinlog";
	}

	@Override
	public String getUsage()
	{
		return "<channel id>";
	}

	@Override
	public String getDescription()
	{
		return "Set or change what channel I send my join logs to.";
	}

	@Override
	public String getPermission()
	{
		return Permissions.ADMIN;
	}
}
