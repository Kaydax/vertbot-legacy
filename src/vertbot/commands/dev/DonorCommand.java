package vertbot.commands.dev;

import vertbot.Main;
import vertbot.commands.Command;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Permissions;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class DonorCommand extends Command
{

	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Guild guild = event.getGuild();
		GuildSettings gs = Main.getGuildSettings(guild.getId());

		// setting log channel
		if (args.size() > 0)
		{
			String tier = args.concat(0);
			Integer psize = 0;
			
			if(tier.equals("penny")) 
			{
				psize = 500;
			} else if(tier.equals("basic"))
			{
				psize = 1000;
			} else if(tier.equals("support"))
			{
				psize = 3000;
			} else if(tier.equals("hero"))
			{
				psize = 5000;
			} else if(tier.equals("gold"))
			{
				psize = 9000;
			} else if(tier.equals("diamond"))
			{
				psize = Integer.MAX_VALUE;
			}
			
			gs.getJSON().put("tier", psize);
			gs.save();

			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Donor tier set");
			ebuilder.setDescription("Tier set to `" + tier + "`");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		} else
		{
			gs.getJSON().remove("tier");
			gs.save();

			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Removed donor tier");
			ebuilder.setDescription("The tier was unset.");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}

	@Override
	public String getName()
	{
		return "donor";
	}

	@Override
	public String getUsage()
	{
		return "<tier>";
	}

	@Override
	public String getDescription()
	{
		return "Sets a server to a donor tier";
	}

	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
}
