package vertbot.commands.dev;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Permissions;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class SayCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		event.getChannel().sendMessage(args.concat()).complete();
		try
		{
			event.getMessage().delete().complete();
		} catch(Exception e) 
		{
			System.out.println("[Debug] Could not delete message");
			return;
		}
	}
	
	@Override
	public String getName()
	{
		return "say";
	}
	
	@Override
	public String getUsage()
	{
		return "<text>";
	}
	
	@Override
	public String getDescription()
	{
		return "Makes me say a thing!";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
}
