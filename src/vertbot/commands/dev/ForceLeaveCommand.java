package vertbot.commands.dev;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Permissions;
import vertbot.util.Responses;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class ForceLeaveCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Guild guild = event.getJDA().getGuildById(args.concat(0));
		Responses.respond(event, "Leaving guild: " + guild.getName());
		guild.leave().complete();
		Responses.respond(event, "Left guild: " + guild.getName());
	}
	
	@Override
	public String getName()
	{
		return "fleave";
	}
	
	@Override
	public String getUsage()
	{
		return "<guild id>";
	}
	
	@Override
	public String getDescription()
	{
		return "Gives you info about what guilds I am on";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
	
}
