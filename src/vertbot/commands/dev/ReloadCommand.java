package vertbot.commands.dev;

import vertbot.Main;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Permissions;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class ReloadCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Main.reloadCommands();
		event.getChannel().sendMessage(event.getAuthor().getAsMention() + " I've reloaded all commands for you.").complete();
	}
	
	@Override
	public String getName()
	{
		return "reload";
	}
	
	@Override
	public String getDescription()
	{
		return "Reload all commands";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
}
