package vertbot.commands.dev;

import java.awt.Color;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Permissions;

public class ServerRulesCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		EmbedBuilder ebuilder = new EmbedBuilder();
		ebuilder.setTitle("Rules of the server:", null);
		ebuilder.addField("1.", "Don't be a dick to anybody.", false);
		ebuilder.addField("2.", "Don't spam bot commands outside of #bot-spam", false);
		ebuilder.addField("3.", "Don't spam or shitpost unless it's inside #shitpost", false);
		ebuilder.addField("4.", "Don't post any nsfw unless inside of #nsfw", false);
		ebuilder.addField("5.", "Don't annoy or ear rape people in voice.", false);
		ebuilder.addField("6.", "Don't spam mention users or staff.", false);
		ebuilder.addField("7.", "If you need staff to help you please mention them once and not 200 times.", false);
		ebuilder.addField("8.", "DO NOT pretend to be a staff member.", false);
		ebuilder.setFooter("There is a 3 strike system. After the third strike you get kicked, 4 strikes is a day ban and 5 is a perm. Strikes reset every month.", null);
		ebuilder.setColor(Color.CYAN);
		MessageEmbed embed = ebuilder.build();
		MessageBuilder mbuilder = new MessageBuilder();
		mbuilder.setEmbed(embed);
		Message message = mbuilder.build();
		event.getChannel().sendMessage(message).complete();
	}
	
	@Override
	public String getName()
	{
		return "rules";
	}
	
	@Override
	public String getUsage()
	{
		return "";
	}
	
	@Override
	public String getDescription()
	{
		return "";
	}	
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
}
