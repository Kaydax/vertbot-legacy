package vertbot.commands.dev;

import java.util.List;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.GetUsers;
import vertbot.util.GuildSorter;
import vertbot.util.Permissions;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class GuildsCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		List<Guild> guilds = event.getJDA().getGuilds();
		String resp = "";
		
		guilds = GuildSorter.sortByUsers(guilds);
		for(int i = 0; i < guilds.size(); i++) 
		{
			Guild guild = guilds.get(i);
			String num = Integer.toString(i + 1);
			
			resp += num + ": " + guild.getName() + "[" + guild.getId() + "] (Users: " + GetUsers.Users(guild) + ") (Bots: " + GetUsers.Bots(guild) + ")";
			resp += (i < guilds.size() - 1 ? "\n" : "");
		}
		
		System.out.println("I'm currently running on " + event.getJDA().getGuilds().size() + " guilds: \n" + resp);
	}
	
	@Override
	public String getName()
	{
		return "guilds";
	}
	
	@Override
	public String getDescription()
	{
		return "Gives you info about what guilds I am on";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
	
}
