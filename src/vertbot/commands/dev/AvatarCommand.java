package vertbot.commands.dev;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Permissions;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.io.File;
import java.io.IOException;

import net.dv8tion.jda.core.entities.Icon;

public class AvatarCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(args.concat(0).toLowerCase().equals("normal")) 
		{
			try {
				event.getJDA().getSelfUser().getManager().setAvatar(Icon.from(new File("Vertbot.png")));
				System.out.println("Normal avatar set");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if(args.concat(0).toLowerCase().equals("april")) 
		{
			try {
				event.getJDA().getSelfUser().getManager().setAvatar(Icon.from(new File("Vertbot-April.png")));
				System.out.println("Lol jokes");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public String getName()
	{
		return "avatar";
	}
	
	@Override
	public String getUsage()
	{
		return "";
	}
	
	@Override
	public String getDescription()
	{
		return "Its a secret to everyone";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
}
