package vertbot.commands.dev;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Permissions;
import vertbot.util.Responses;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class NicknameCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			Member self = event.getGuild().getSelfMember();
			if(args.size() > 0)
			{
				String nick = args.concat();
				
				event.getGuild().getController().setNickname(self, nick).complete();
				Responses.respond(event, "Nickname set to '" + nick + "'");
			}
			else
			{
				event.getGuild().getController().setNickname(self, null).complete();
				Responses.respond(event, "Nickname cleared");
			}
		}
		catch(Exception e)
		{
			Responses.respond(event, "Could not set nickname.");
		}
	}
	
	@Override
	public String getName()
	{
		return "nick";
	}
	
	@Override
	public String getUsage()
	{
		return "<name>";
	}
	
	@Override
	public String getDescription()
	{
		return "Change my nickname";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
	
}
