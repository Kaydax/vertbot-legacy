package vertbot.commands.dev;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.GetUsers;
import vertbot.util.Permissions;
import vertbot.util.Responses;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class WhoServerCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			Guild guild = event.getJDA().getGuildById(args.concat(0));
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle(guild.getName() + " info:", null);
			ebuilder.setImage(guild.getIconUrl());
			ebuilder.setDescription("\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_");
			ebuilder.addField("Guild ID:", guild.getId(), true);
			ebuilder.addField("Guild Owner:", guild.getOwner().getUser().getName() + "#" + guild.getOwner().getUser().getDiscriminator(), true);
			ebuilder.addField("Guild Owner ID:", guild.getOwner().getUser().getId(), true);
			ebuilder.addField("Users:", "" + GetUsers.Users(guild), true);
			ebuilder.addField("Bots:", "" + GetUsers.Bots(guild), true);
			ebuilder.setColor(ColorUtil.randomColor());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		catch(Exception e)
		{
			Responses.respond(event, "Please put a valid id.");
		}
	}
	
	@Override
	public String getName()
	{
		return "serverinfo";
	}
	
	@Override
	public String getUsage()
	{
		return "<server id>";
	}
	
	@Override
	public String getDescription()
	{
		return "Gives you a servers info";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
	
}
