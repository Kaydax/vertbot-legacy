package vertbot.commands;

import vertbot.util.Args;
import vertbot.util.ColorUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class DonateCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		EmbedBuilder ebuilder = new EmbedBuilder();
		ebuilder.setColor(ColorUtil.neutral());
		ebuilder.addField("Patreon and Paypal:", "[Patreon](https://www.patreon.com/vertbot) or [Paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUEV4DTNB8LQJ)", false);
		ebuilder.addField("All the crypto currancy I support:", 
				"```Bitcoin: 1Jy7wqGmf1vo4nu43YDsgNYSBaRAiArN3d"
				+ "\nEthereum: 0xd1e064F887ea42095A6aDa3A4A9A564c75cb9e6A"
				+ "\nDogecoin: DRFkMN6wkhg3tAFkSjWE5iYn1mfeaAuseF"
				+ "\nLitecoin: LUzWsb1ZPx7uD4dhK33FHZjLVXsSApY7eZ"
				+ "\nAugur: 0xd1e064F887ea42095A6aDa3A4A9A564c75cb9e6A"
				+ "\nAragon: 0xd1e064F887ea42095A6aDa3A4A9A564c75cb9e6A"
				+ "\nDash: XsCU2MAK1brUKZPHoUALW9sTJbMEywy5Wa"
				+ "\nDecred: DsXjoFG4UpH7bJLXch5XzLZkJ9jcFei8LpV"
				+ "\nGolem: 0xd1e064F887ea42095A6aDa3A4A9A564c75cb9e6A"
				+ "\nEOS: 0xd1e064F887ea42095A6aDa3A4A9A564c75cb9e6A"
				+ "\nGnosis: 0xd1e064F887ea42095A6aDa3A4A9A564c75cb9e6A```", false);
		MessageEmbed embed = ebuilder.build();
		MessageBuilder mbuilder = new MessageBuilder();
		mbuilder.setEmbed(embed);
		Message message = mbuilder.build();
		event.getChannel().sendMessage(message).complete();
	}
	
	@Override
	public String getName()
	{
		return "donate";
	}
	
	@Override
	public String getDescription()
	{
		return "Gives you info about donating";
	}
	
}
