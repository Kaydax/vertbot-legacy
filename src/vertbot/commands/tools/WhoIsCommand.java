package vertbot.commands.tools;

//import java.io.PrintWriter;
//import java.io.StringWriter;
import java.time.format.DateTimeFormatter;
import java.util.List;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class WhoIsCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			User user;
			List<User> userList = event.getMessage().getMentionedUsers();
			if(userList.size() <= 0) {
				user = event.getAuthor();
			} else {
				user = userList.get(0);
			}
			Guild guild = event.getGuild();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E, MMM dd, yyyy");
			EmbedBuilder ebuilder = new EmbedBuilder();
			String roles = "";
			String nick = "";
			if(guild.getMember(user).getNickname() != null)
			{
				nick = guild.getMember(user).getNickname();
			}
			else
			{
				nick = user.getName();
			}
			List<Role> roleList = guild.getMember(user).getRoles();
			if(roleList.size() == 0)
			{
				roles = "No roles for this user";
			}
			for(int i = 0; i < guild.getMember(user).getRoles().size(); i++)
			{
				roles += roleList.get(i).getName();
				roles += i == roleList.size() - 1 ? "" : ", ";
			}
			ebuilder.setTitle(user.getName() + "#" + user.getDiscriminator() + "'s info:", null);
			ebuilder.setThumbnail(user.getAvatarUrl());
			ebuilder.setDescription("\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_\\_");
			ebuilder.addField("Discord Name:", user.getName(), true);
			ebuilder.addField("Nickname:", nick, true);
			ebuilder.addField("User's unique ID:", user.getId(), true);
			ebuilder.addField("Join Date:", guild.getMember(user).getJoinDate().format(formatter), true);
			ebuilder.addField("Account Creation:", user.getCreationTime().format(formatter), true);
			ebuilder.addField("Roles:", roles, false);
			ebuilder.setColor(ColorUtil.randomColor());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		catch(Exception e)
		{
			//StringWriter sw = new StringWriter();
			//e.printStackTrace(new PrintWriter(sw));
			//String es = sw.toString();
			//event.getChannel().sendMessage(event.getAuthor().getAsMention() + es).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "whois";
	}
	
	@Override
	public String getUsage()
	{
		return "<@user>";
	}
	
	@Override
	public String getDescription()
	{
		return "Gives you a person's info";
	}
	
}
