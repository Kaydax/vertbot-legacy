package vertbot.commands.tools;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Responses;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class PicCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try {
			String resp = event.getMessage().getMentionedUsers().get(0).getId();
			Responses.respond(event, event.getJDA().getUserById(resp).getAvatarUrl() + "?size=2048");
		} catch(Exception e) {
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: You need to mention someone");
			ebuilder.setDescription("You need to mention someone to get their picture.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "avatar";
	}
	
	@Override
	public String getDescription()
	{
		return "Gives you a person's avatar";
	}
	
}
