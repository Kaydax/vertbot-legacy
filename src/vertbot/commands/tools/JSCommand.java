package vertbot.commands.tools;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Processes;
import vertbot.util.Responses;
import vertbot.util.Utils;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class JSCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		String resp = Processes.node("sandbox", Utils.quotify(args.concat(0, -1, false)));
		
		Responses.respond(event, "```\n" + Utils.clip(resp, 1900) + "```");
	}
	
	@Override
	public String getName()
	{
		return "js";
	}
	
	@Override
	public String getUsage()
	{
		return "<code>";
	}
	
	@Override
	public String getDescription()
	{
		return "Run some JavaScript";
	}
	
	@Override
	public String getPermission()
	{
		return "";//Permissions.DEV;
	}
	
}
