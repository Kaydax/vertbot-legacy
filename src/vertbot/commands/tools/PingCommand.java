package vertbot.commands.tools;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class PingCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
    	Responses.respond(event, "Bing Bing Bong. My current ping is " + event.getJDA().getPing() + "ms");
	}
	
	@Override
	public String getName()
	{
		return "ping";
	}
	
	@Override
	public String getUsage()
	{
		return "";
	}
	
	@Override
	public String getDescription()
	{
		return "Check if I'm here";
	}
	
}
