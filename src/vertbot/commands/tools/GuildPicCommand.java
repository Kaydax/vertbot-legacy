package vertbot.commands.tools;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class GuildPicCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Responses.respond(event, event.getGuild().getIconUrl());
	}
	
	@Override
	public String getName()
	{
		return "guildavatar";
	}
	
	@Override
	public String getDescription()
	{
		return "Gives you the guilds avatar";
	}
	
}
