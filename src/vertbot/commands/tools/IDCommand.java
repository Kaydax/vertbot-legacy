package vertbot.commands.tools;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Permissions;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class IDCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{		
		String id = event.getMessage().getMentionedUsers().get(0).getId();
		Responses.respond(event, "The ID for this user is: " + id);
	}
	
	@Override
	public String getName()
	{
		return "id";
	}
	
	@Override
	public String getUsage()
	{
		return "@<user>";
	}
	
	@Override
	public String getDescription()
	{
		return "Gets a id of a user";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.ADMIN;//Permissions.DEV;
	}
	
}
