package vertbot.commands;

import vertbot.util.Args;
import vertbot.util.ColorUtil;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class JoinCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		EmbedBuilder ebuilder = new EmbedBuilder();
		ebuilder.addField("My bot invite link:", "[Invite me!](https://discordapp.com/oauth2/authorize?client_id=316520238835433482&scope=bot&permissions=36793352)", true);
		ebuilder.addField("My support Server:", "[Join My server!](https://discord.gg/WPUU2dF)", true);
		ebuilder.setColor(ColorUtil.neutral());
		MessageEmbed embed = ebuilder.build();
		MessageBuilder mbuilder = new MessageBuilder();
		mbuilder.setEmbed(embed);
		Message message = mbuilder.build();
		event.getChannel().sendMessage(message).complete();
	}
	
	@Override
	public String getName()
	{
		return "invite";
	}
	
	@Override
	public String getDescription()
	{
		return "Sends a link to my discord guild where you can get more info";
	}
	
}
