package vertbot.commands;

import java.util.Arrays;
import vertbot.Main;
import vertbot.music.MusicPlayer;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDAInfo;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class InfoCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		String dev = "";
		String prefix = "";
		
		if(Main.dev == true) {
			prefix = "[=";
			dev = " - Development Build";
		} else {
			prefix = "-";
		}
		
		final long serv = Arrays.stream(Main.getShards())
                .filter(s ->  s != null && s.jda != null)
                .map(s -> s.jda.getGuilds().size())
                .reduce(0, (a, b) -> a + b);
		
		int maxshards = Main.shards.length;
		
		final long streams = Main.players.values().stream().filter(MusicPlayer::isPlaying).count();
		
		Runtime rt = Runtime.getRuntime();
		int mb = 1024*1024;
		int gb = 1024*1024*1024;
		long used = (rt.totalMemory() - rt.freeMemory()) / mb;
		String usedmem = "";
		if(used >= 1024) {
			usedmem = (rt.totalMemory() - rt.freeMemory()) / gb + " gb";
		} else {
			usedmem = used + " mb";
		}
		GuildSettings gs = Main.getGuildSettings(event.getGuild().getId());
		EmbedBuilder ebuilder = new EmbedBuilder();
		ebuilder.setTitle("Hello, I am Vertbot!", null);
		ebuilder.setColor(ColorUtil.neutral());
		ebuilder.setAuthor(event.getJDA().getSelfUser().getName(), null, event.getJDA().getSelfUser().getAvatarUrl());
		ebuilder.setDescription("Vertbot is a Discord bot made using JDA. Its main purpose is to be one of the only Discord bots that give people the power to give little lag music playback with very few limitations and have fun commands to anyone to mess around with.");
		ebuilder.addField("Current prefix:", gs.getJSON().optString("prefix", prefix), true);
		ebuilder.addField("Servers:", "" + serv, true);
		ebuilder.addField("Shard:", "" + (event.getJDA().getShardInfo().getShardId() + 1) + " / " + maxshards, true);
		ebuilder.addField("Current Build:", Main.version + dev, true);
		ebuilder.addField("Current JDA Build:", JDAInfo.VERSION, true);
		//ebuilder.addField("Hosted By:", "[Denk IT](https://www.denkit.com/)", false);
		ebuilder.addField("My Discord:", "[Join Now](https://discord.gg/WPUU2dF)", true);
		ebuilder.addField("Servers using music:", "" + streams, true);
		ebuilder.addField("Ping:", event.getJDA().getPing() + "ms", true);
		ebuilder.addField("RAM usage:", usedmem, true);
		ebuilder.setFooter("Developers: Kaydax#8965 and tehZevo#0321", null);
		MessageEmbed embed = ebuilder.build();
		MessageBuilder mbuilder = new MessageBuilder();
		mbuilder.setEmbed(embed);
		Message message = mbuilder.build();
		event.getChannel().sendMessage(message).complete();
	}
	
	@Override
	public String getName()
	{
		return "info";
	}
	
	@Override
	public String getDescription()
	{
		return "Gives you info about me";
	}
	
}
