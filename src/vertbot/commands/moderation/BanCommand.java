package vertbot.commands.moderation;

import vertbot.Main;
import vertbot.commands.Command;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Permissions;
import vertbot.util.Utils;

import java.awt.Color;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class BanCommand extends Command
{

	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		GuildSettings gs = Main.getGuildSettings(event.getGuild().getId());
		User user;
		List<User> userList = event.getMessage().getMentionedUsers();
		if (userList.size() <= 0)
		{
			user = event.getAuthor();
		} else
		{
			user = userList.get(0);
		}
		try
		{
			event.getGuild().getController().ban(event.getGuild().getMember(user), 0).complete();
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: " + user.getName() + " has been ban");
			ebuilder.setDescription("The member has been ban without any problem");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message msg = mbuilder.build();
			event.getChannel().sendMessage(msg).complete();
			try
			{
				System.out.println(Utils.debugOut(event, true));
				EmbedBuilder ebuilderl = new EmbedBuilder();
				ebuilderl.setTitle("Member has been ban by " + event.getAuthor().getName());
				ebuilderl.setDescription(event.getAuthor().getName() + " has ban: " + user.getName());
				ebuilderl.setColor(Color.RED);
				MessageEmbed embedl = ebuilderl.build();
				MessageBuilder mbuilderl = new MessageBuilder();
				mbuilderl.setEmbed(embedl);
				Message msgl = mbuilderl.build();
				event.getGuild().getTextChannelById(gs.getJSON().optString("log", "")).sendMessage(msgl).complete();
			} catch (Exception e)
			{
				return;
			}
		} catch (Exception e)
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: " + user.getName() + " couldn't be banned");
			ebuilder.setDescription("The member couldn't be banned. This may have something to do with my perms.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message msg = mbuilder.build();
			event.getChannel().sendMessage(msg).complete();
		}
	}

	@Override
	public String getName()
	{
		return "ban";
	}

	@Override
	public String getUsage()
	{
		return "{mention}";
	}

	@Override
	public String getDescription()
	{
		return "Bans a uses and makes life easy";
	}
	@Override
	public String getPermission()
	{
		return Permissions.ADMIN;
	}
}
