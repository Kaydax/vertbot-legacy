package vertbot.commands.moderation;

import vertbot.Main;
import vertbot.commands.Command;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Permissions;
import vertbot.util.Utils;

import java.awt.Color;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class UnBanCommand extends Command
{

	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		GuildSettings gs = Main.getGuildSettings(event.getGuild().getId());
		User user = event.getJDA().getUserById(args.concat(0));
		try
		{
			event.getGuild().getController().unban(user);
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: " + user.getName() + " has been unban");
			ebuilder.setDescription("The member has been unban without any problem");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message msg = mbuilder.build();
			event.getChannel().sendMessage(msg).complete();
			try
			{
				System.out.println(Utils.debugOut(event, true));
				EmbedBuilder ebuilderl = new EmbedBuilder();
				ebuilderl.setTitle("Member has been unban by " + event.getAuthor().getName());
				ebuilderl.setDescription(event.getAuthor().getName() + " has unban: " + user.getName());
				ebuilderl.setColor(Color.RED);
				MessageEmbed embedl = ebuilderl.build();
				MessageBuilder mbuilderl = new MessageBuilder();
				mbuilderl.setEmbed(embedl);
				Message msgl = mbuilderl.build();
				event.getGuild().getTextChannelById(gs.getJSON().optString("log", "")).sendMessage(msgl).complete();
			} catch (Exception e)
			{
				return;
			}
		} catch (Exception e)
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: " + user.getName() + " could not be unban");
			ebuilder.setDescription("The member could not be unban. This may have something to do with my perms.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message msg = mbuilder.build();
			event.getChannel().sendMessage(msg).complete();
		}
	}

	@Override
	public String getName()
	{
		return "unban";
	}

	@Override
	public String getUsage()
	{
		return "{user id}";
	}

	@Override
	public String getDescription()
	{
		return "Unbans a uses and makes life easy";
	}
	@Override
	public String getPermission()
	{
		return Permissions.ADMIN;
	}
}
