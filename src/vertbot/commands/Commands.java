package vertbot.commands;

public class Commands extends Command
{
	public Commands()
	{
		this.addCommand(new HelpCommand());
		this.addCommand(new JoinCommand());
		this.addCommand(new InfoCommand());
		this.addCommand(new DonateCommand());
		this.addCommand(new SettingsCommand());
		this.addCommand(new DevCommand());
		this.addCommand(new FunCommand());
		this.addCommand(new ToolsCommand());
		this.addCommand(new MusicCommand());
		this.addCommand(new AdminCommand());
		this.addCommand(new FixCommand());
	}
	
	@Override
	public String getName()
	{
		return "";
	}
	
}
