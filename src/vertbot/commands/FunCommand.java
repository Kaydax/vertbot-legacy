package vertbot.commands;

import java.util.HashMap;

import vertbot.commands.fun.AsciiCommand;
import vertbot.commands.fun.Base64Command;
import vertbot.commands.fun.CoinTossCommand;
import vertbot.commands.fun.ComicCommand;
import vertbot.commands.fun.GelbooruCommand;
import vertbot.commands.fun.HexCommand;
import vertbot.commands.fun.LMGTFYCommand;
import vertbot.commands.fun.MD5Command;
import vertbot.commands.fun.MazeCommand;
import vertbot.commands.fun.MemeCommand;
import vertbot.commands.fun.SkinCommand;
import vertbot.commands.fun.UrbanCommand;
import vertbot.commands.fun.VaporCommand;
import vertbot.commands.fun.XkcdCommand;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class FunCommand extends Command
{
	public FunCommand()
	{
		super();
		
		this.addCommand(new AsciiCommand());
		this.addCommand(new CoinTossCommand());
		this.addCommand(new MazeCommand());
		this.addCommand(new ComicCommand());
		this.addCommand(new HexCommand());
		this.addCommand(new LMGTFYCommand());
		this.addCommand(new Base64Command());
		this.addCommand(new MD5Command());
		this.addCommand(new VaporCommand());
		this.addCommand(new SkinCommand());
		this.addCommand(new UrbanCommand());
		this.addCommand(new GelbooruCommand());
		this.addCommand(new XkcdCommand());
		this.addCommand(new MemeCommand());
	}
	
	static HashMap<String, GuildSettings> guildSettingsCache = new HashMap<>();
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.isFromType(ChannelType.PRIVATE))
		{
			Responses.noPrivate(event);
			
			return;
		}
		
		if(args.size() == 0)
		{
			Responses.respond(event, "```" + new HelpCommand().getHelp(event, this) + "```");
			return;
		}
		
		super.doCommand(event, args);
		
		/*if(args.concat().length() == 0) {
			GuildSettings gs = getGuildSettings(event.getGuild().getId());
			Responses.respond(event, "This command has sub-commands. Please do '" + gs.getJSON().optString("prefix", "") + "help fun' to see the sub-commands");
		}*/
	}
	
	@Override
	public String getName()
	{
		return "fun";
	}
	
	@Override
	public String getDescription()
	{
		return "Fun Commands <Sub-menu>";
	}
	
	@Override
	public String getPermission()
	{
		return "";
	}
	
}
