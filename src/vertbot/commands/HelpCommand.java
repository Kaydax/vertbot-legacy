package vertbot.commands;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import vertbot.Main;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class HelpCommand extends Command
{
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		String help = this.getHelp(event, args);
		
		Responses.respond(event, "```" + help + "```");
	}
	
	/** returns the best matching command from the given arguments */
	public Command getCommandFromArgs(Args args)
	{
		Command command = Main.getCommands();
		
		//walk down the arg list, trying to find the appropriate subcommand
		for(int i = 0; i < args.size(); i++)
		{
			Command subCommand = command.getCommand(args.get(i));
			
			//if subcommand is not null, continue walking
			if(subCommand != null)
			{
				command = subCommand;
			}
			//otherwise weve found the command they need help for ;)
			else
			{
				break;
			}
		}
		
		return command;
	}
	
	public String getHelp(MessageReceivedEvent event, Args args)
	{
		return this.getHelp(event, this.getCommandFromArgs(args));
	}
	
	/** formats help for all subcommands of the given command */
	public String getHelp(MessageReceivedEvent event, Command command)
	{
		//determine width of table columns (in characters)
		int permWidth = 0;
		int nameWidth = 0;
		int usageWidth = 0;
		int nameUsageWidth = 0;
		int descWidth = 0;
		
		List<Entry<String, Command>> matches = command.getMatchingCommands(null, null); //TODO: restrict by perm again
		
		//for(Iterator<Entry<String, Command>> iter = command.subCommands.entrySet().iterator(); iter.hasNext();)
		for(Iterator<Entry<String, Command>> iter = matches.iterator(); iter.hasNext();)
		{
			Entry<String, Command> e = iter.next();
			
			String name = e.getKey();
			Command subCommand = e.getValue();
			
			permWidth = Math.max(permWidth, subCommand.getPermission().length());
			nameWidth = Math.max(nameWidth, name.length());
			usageWidth = Math.max(usageWidth, subCommand.getUsage().length());
			nameUsageWidth = Math.max(nameUsageWidth, name.length() + subCommand.getUsage().length() + 1);
			descWidth = Math.max(descWidth, subCommand.getDescription().length());
		}
		
		String help = "";
		
		//add all subcommand usages
		for(Iterator<Entry<String, Command>> iter = command.subCommands.entrySet().iterator(); iter.hasNext();)
		{
			Entry<String, Command> e = iter.next();
			
			String name = e.getKey();
			Command subCommand = e.getValue();
			
			if(subCommand.isVisible())
			{
				//help += "" + String.format("%-" + nameUsageWidth + "s | %-" + descWidth + "s %-" + permWidth + 2 + "s", name + " " + subCommand.getUsage(), subCommand.getDescription(), "[" + subCommand.getPermission() + "]");
				help += String.format("%-" + nameUsageWidth + "s", name + " " + subCommand.getUsage()) + " | ";
				help += String.format("%-" + descWidth + "s", subCommand.getDescription()) + " ";
				if(subCommand.getPermission().length() > 0)
				{
					help += String.format("%-" + permWidth + 2 + "s", "[" + subCommand.getPermission() + "]");
				}
				help += iter.hasNext() ? "\n" : "";
			}
		}
		
		return help;
	}
	
	@Override
	public String getName()
	{
		return "help";
	}
	
	@Override
	public String getUsage()
	{
		return "<command>";
	}
	
	@Override
	public String getDescription()
	{
		return "Get help for commands";
	}
	
}
