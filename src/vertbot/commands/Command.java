package vertbot.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.util.Args;
import vertbot.util.Permissions;
import vertbot.util.Responses;
import vertbot.util.Utils;

public abstract class Command
{
	TreeMap<String, Command> subCommands;
	
	Command parent;
	
	public Command()
	{
		this.subCommands = new TreeMap<String, Command>();
	}
	
	/** returns the name of this command */
	public abstract String getName();
	
	public TreeMap<String, Command> getSubCommands()
	{
		return this.subCommands;
	}
	
	public int getLevel()
	{
		Command cmd = this;
		int level = 0;
		
		while(cmd != null)
		{
			level++;
			cmd = cmd.getParent();
		}
		
		return level;
	}
	
	public String getPath()
	{
		String path = "";
		Command cmd = this;
		
		while(cmd != null)
		{
			path = cmd.getName() + " " + path;
			cmd = cmd.getParent();
		}
		
		path = path.trim();
		
		return path;
	}
	
	/** override me TODO: break these up into an internal command that calls subcommands(if found), otherwise calls onGuild/onPrivate */
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			//if theres no sub commands, then display help
			if(args.size() == 0)
			{
				//Responses.respond(event, "```" + new HelpCommand().getHelp(event, this) + "```");
				return;
			}
			
			String name = args.get(0).toLowerCase();
			
			//breadth first search:
			ArrayList<Command> cmds = new ArrayList<Command>();
			cmds.addAll(this.subCommands.values());
			
			ArrayList<Command> found = new ArrayList<Command>();
			
			int foundLevel = -1;
			
			while(cmds.size() > 0)
			{
				Command tmp = cmds.remove(0);
				
				if(tmp.getName().startsWith(name))
				{
					int level = tmp.getLevel();
					
					if(foundLevel == -1)
					{
						foundLevel = level;
					}
					
					if(level == foundLevel)
					{
						found.add(tmp);
					}
					//we've gone up a level
					else
					{
						break;
					}
				}
				
				//dont add sub commands who we lack permission from parent
				if(tmp.checkPermission(event))
				{
					cmds.addAll(tmp.getSubCommands().values());
				}
			}
			
			if(found.size() == 0)
			{
				//Responses.help(event, this);
				
				return;
			}
			
			if(found.size() > 1)
			{
				String resp = "";
				
				for(int i = 0; i < found.size(); i++)
				{
					Command match = found.get(i);
					
					if(match.isVisible())
					{
						resp += "'" + found.get(i).getPath() + "'";//found.get(i).getName();
						resp += i < found.size() - 1 ? ", " : "";
					}
				}
				
				//TODO: rematch with permissions to hide them here?
				Responses.respond(event, "I know several commands starting with '" + name + "':\n```" + resp + "```");
				
				return;
			}
			
			Command command = found.get(0);
			Args newArgs = new Args(args.rest());
			
			if(!command.checkPermission(event))
			{
				Responses.noPermission(event, command.getPermission());
			}
			else
			{
				command.doCommand(event, newArgs);
			}
		}
		catch(Exception e)
		{
			return;
		}
	}
	
	/** override me */
	public String getUsage()
	{
		return "";
	}
	
	/** override me */
	public String getDescription()
	{
		return "";
	}
	
	/** override me */
	public String getPermission()
	{
		return "";
	}
	
	/** override me; if false, command will not appear in help */
	public boolean isVisible()
	{
		return true;
	}
	
	public boolean checkPermission(MessageReceivedEvent event)
	{
		String perm = this.getPermission();
		User user = event.getAuthor();
		
		boolean isDev = user.getId().equals(Main.DEV_ID);
		boolean isOwner = user.getId().equals(event.getGuild().getOwner().getUser().getId());
		boolean isAdmin = Utils.getPerms(event, event.getGuild().getMember(event.getAuthor())).contains(Permission.ADMINISTRATOR);
		boolean isManager = Utils.getPerms(event, event.getGuild().getMember(event.getAuthor())).contains(Permission.MANAGE_SERVER);
		
		if(perm.equals(Permissions.DEV))
		{
			return isDev;
		}
		else if(perm.equals(Permissions.OWNER))
		{
			return isDev || isOwner;
		}
		else if(perm.equals(Permissions.ADMIN))
		{
			return isDev || isOwner || isAdmin;
		}
		else if(perm.equals(Permissions.MANAGER))
		{
			return isDev || isOwner || isAdmin || isManager;
		}
		else
		{
			return true;
		}
	}
	
	public void addCommand(Command command)
	{
		this.subCommands.put(command.getName(), command);
		
		command.setParent(this);
	}
	
	public void setParent(Command command)
	{
		this.parent = command;
	}
	
	public Command getParent()
	{
		return this.parent;
	}
	
	public void removeCommand(String name)
	{
		this.subCommands.remove(name);
	}
	
	/** if event is non-null, checks for permission, if string is non-null, filter by name */
	public List<Entry<String, Command>> getMatchingCommands(MessageReceivedEvent event, String name)
	{
		boolean filterPerm = (event != null);
		boolean filterName = (name != null);
		
		ArrayList<Entry<String, Command>> matches = new ArrayList<>();
		
		//if theyre both null, just copy all commands into the list and return
		if(!filterPerm && !filterName)
		{
			matches.addAll(this.subCommands.entrySet());
			
			return matches;
		}
		
		for(Entry<String, Command> e : this.subCommands.entrySet())
		{
			String subName = e.getKey();
			Command subCommand = e.getValue();
			
			//if command fails permission check, skip
			if(filterPerm && !subCommand.checkPermission(event))
			{
				continue;
			}
			//if command fails name check, skip
			if(filterName && !subName.startsWith(name.toLowerCase()))
			{
				continue;
			}
			
			//if its an exact match, just return that one
			if(subName.equals(name))
			{
				matches.clear();
				matches.add(e);
				
				return matches;
			}
			
			//otherwise, add it to the list
			matches.add(e);
		}
		
		return matches;
	}
	
	/** returns the subcommand that matches the given name, or null if ambiguous/not found (ignoring permission) */
	public Command getCommand(String name)
	{
		List<Entry<String, Command>> matches = this.getMatchingCommands(null, name);
		
		//System.out.println(this.getClass().getName() + " matches for " + name + ": " + matches.size());
		
		if(matches.size() == 1)
		{
			return matches.get(0).getValue();
		}
		else
		{
			return null;
		}
	}
	
}
