package vertbot.commands;

import java.util.HashMap;

import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.commands.dev.AvatarCommand;
import vertbot.commands.dev.DonorCommand;
import vertbot.commands.dev.ForceLeaveCommand;
import vertbot.commands.dev.GuildsCommand;
import vertbot.commands.dev.NicknameCommand;
import vertbot.commands.dev.ReloadCommand;
import vertbot.commands.dev.SayCommand;
import vertbot.commands.dev.ServerRulesCommand;
import vertbot.commands.dev.WhoServerCommand;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.Permissions;
import vertbot.util.Responses;

public class DevCommand extends Command
{
	public DevCommand()
	{
		super();
		
		this.addCommand(new ReloadCommand());
		this.addCommand(new SayCommand());
		this.addCommand(new GuildsCommand());
		this.addCommand(new NicknameCommand());
		this.addCommand(new ServerRulesCommand());
		this.addCommand(new ForceLeaveCommand());
		this.addCommand(new WhoServerCommand());
		this.addCommand(new DonorCommand());
		this.addCommand(new AvatarCommand());
	}
	
	static HashMap<String, GuildSettings> guildSettingsCache = new HashMap<>();
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.isFromType(ChannelType.PRIVATE))
		{
			Responses.noPrivate(event);
			
			return;
		}
		
		if(args.size() == 0)
		{
			Responses.respond(event, "```" + new HelpCommand().getHelp(event, this) + "```");
			return;
		}
		
		super.doCommand(event, args);
		
		/*if(args.concat().length() == 0) {
			GuildSettings gs = getGuildSettings(event.getGuild().getId());
			Responses.respond(event, "This command has sub-commands. Please do '" + gs.getJSON().optString("prefix", "") + "help dev' to see the sub-commands. *But you can't use any anyway kek*");
		}*/
	}
	
	@Override
	public String getName()
	{
		return "dev";
	}
	
	@Override
	public String getDescription()
	{
		return "Dev Commands <Sub-menu>";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.DEV;
	}
	
}
