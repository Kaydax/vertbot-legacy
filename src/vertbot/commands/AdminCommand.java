package vertbot.commands;

import java.util.HashMap;
import vertbot.commands.moderation.BanCommand;
import vertbot.commands.moderation.KickCommand;
import vertbot.commands.moderation.UnBanCommand;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.Permissions;
import vertbot.util.Responses;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class AdminCommand extends Command
{
	public AdminCommand()
	{
		super();
		
		this.addCommand(new BanCommand());
		this.addCommand(new KickCommand());
		this.addCommand(new UnBanCommand());
	}
	
	static HashMap<String, GuildSettings> guildSettingsCache = new HashMap<>();
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.isFromType(ChannelType.PRIVATE))
		{
			Responses.noPrivate(event);
			
			return;
		}
		
		if(args.size() == 0)
		{
			Responses.respond(event, "```" + new HelpCommand().getHelp(event, this) + "```");
			return;
		}
		
		super.doCommand(event, args);
		
		/*if(args.concat().length() == 0) {
			GuildSettings gs = getGuildSettings(event.getGuild().getId());
			Responses.respond(event, "This command has sub-commands. Please do '" + gs.getJSON().optString("prefix", "") + "help fun' to see the sub-commands");
		}*/
	}
	
	@Override
	public String getName()
	{
		return "admin";
	}
	
	@Override
	public String getDescription()
	{
		return "Admin Commands <Sub-menu>";
	}
	
	@Override
	public String getPermission()
	{
		return Permissions.ADMIN;
	}
	
}
