package vertbot.commands.music;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.music.MusicPlayer;
import vertbot.util.Args;
import vertbot.util.ColorUtil;

public class PlayCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			if(args.concat().length() > 0)
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Use add command");
				ebuilder.setDescription("Please use the add command in order to add things, then use this command to play the playlist.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
			else
			{
				MusicPlayer player = Main.getPlayer(event.getGuild());
				if(player.getPlaylist().index > 0)
				{
					player.getPlaylist().index = 0;
				}
				if(player.getPlaylist().getList().size() != 0)
				{
					player.purge();
					player.setEvent(event);
					VoiceChannel voice = event.getGuild().getMember(event.getAuthor()).getVoiceState().getChannel();
					event.getGuild().getAudioManager().openAudioConnection(voice);
					player.Pause(false);
					player.Play();
					if(player.getPlaying() != null)
					{
						EmbedBuilder ebuilder = new EmbedBuilder();
						ebuilder.addField("Now Playing:", "`" + Integer.toString(player.getPlaylist().index + 1) + ": " + Main.getPlayer(event.getGuild()).getPlaying().title + "`", false);
						ebuilder.setColor(ColorUtil.neutral());
						MessageEmbed embed = ebuilder.build();
						MessageBuilder mbuilder = new MessageBuilder();
						mbuilder.setEmbed(embed);
						Message message = mbuilder.build();
						event.getChannel().sendMessage(message).complete();
					}
				}
				else
				{
					EmbedBuilder ebuilder = new EmbedBuilder();
					ebuilder.setTitle("Error: Nothing to play");
					ebuilder.setDescription("There is nothing added to the playlist to play.");
					ebuilder.setColor(ColorUtil.error());
					MessageEmbed embed = ebuilder.build();
					MessageBuilder mbuilder = new MessageBuilder();
					mbuilder.setEmbed(embed);
					Message message = mbuilder.build();
					event.getChannel().sendMessage(message).complete();
				}
			}
		}
		catch(Exception e)
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: Voice connection problem");
			ebuilder.setDescription("I could not connect to the voice channel, there may be a permission problem or you are not connected to one.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "play";
	}
	
	@Override
	public String getUsage()
	{
		return "";
	}
	
	@Override
	public String getDescription()
	{
		return "Plays the playlist.";
	}
	
}
