package vertbot.commands.music;

//import java.io.PrintWriter;
//import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.music.MusicPlayer;
import vertbot.music.Playlist;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Responses;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;

public class PlaylistCommand extends Command
{

	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			String zero = "";
			String zerol = "";
			String list = "";
			String playing = "";
			String playnum = "";
			GuildSettings gs = Main.getGuildSettings(event.getGuild().getId());
			MusicPlayer player = Main.getPlayer(event.getGuild());
			Playlist playlist = player.getPlaylist();
			int length = playlist.getList().size();
			int MAX_COUNT = 15;
			int start = Math.max(0, playlist.index - MAX_COUNT / 2);
			int end = Math.min(start + MAX_COUNT, playlist.getList().size());
			long posm;
			long poss;
			long m;
			long s;
			if(player.getPlayer().getPlayingTrack() == null)
			{
				posm = 0;
				poss = 0;
				m = 0;
				s = 0;
				playing = "Nothing";
				playnum = "";
			}
			else
			{
				posm = TimeUnit.MILLISECONDS.toMinutes(player.getPlayer().getPlayingTrack().getPosition());
				poss = TimeUnit.MILLISECONDS.toSeconds(player.getPlayer().getPlayingTrack().getPosition()) % 60;
				m = TimeUnit.MILLISECONDS.toMinutes(player.getPlaying().length);
				s = TimeUnit.MILLISECONDS.toSeconds(player.getPlaying().length) % 60;
				playing = player.getPlaying().title;
				playnum = Integer.toString(playlist.index + 1) + ": ";
			}

			for(int i = start; i < end; i++)
			{
				AudioTrack track = playlist.getList(i);
				AudioTrackInfo info = track.getInfo();

				String title = info.title;
				if(title.length() > 50)
				{
					title = title.substring(0, 50) + "...";
				}

				String add = "" + String.format("%-7s", (i == playlist.index ? "> " : "  ") + (i + 1) + ":") + title + "\n";

				list += add;
			}

			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Playlist:", null);
			if(poss < 10)
			{
				zero = "0";
			}
			else
			{
				zero = "";
			}
			if(s < 10)
			{
				zerol = "0";
			}
			else
			{
				zerol = "";
			}
			ebuilder.addField("Now Playing:", "" + playnum + playing, true);
			ebuilder.addField("Time:", posm + ":" + zero + poss + " / " + m + ":" + zerol + s, false);
			ebuilder.addField("Modes:", "```Repeat: " + gs.getJSON().optString("repeat", "off") + ", Shuffle: " + gs.getJSON().optString("shuffle", "off") + ", Silent: " + gs.getJSON().optString("silent", "off") + "```", false);
			ebuilder.setColor(ColorUtil.randomColor());
			ebuilder.setDescription("```markdown\n" + list + "\n{" + Integer.toString(length) + " videos in total}```");
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		catch(Exception e)
		{
			//StringWriter sw = new StringWriter();
			//e.printStackTrace(new PrintWriter(sw));
			//String es = sw.toString();
			//event.getChannel().sendMessage(event.getAuthor().getAsMention() + es).complete();
			Responses.respond(event, "Could not send embed.");
		}
	}

	@Override
	public String getName()
	{
		return "list";
	}

	@Override
	public String getUsage()
	{
		return "";
	}

	@Override
	public String getDescription()
	{
		return "Shows you the current playlist.";
	}

}
