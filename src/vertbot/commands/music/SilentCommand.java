package vertbot.commands.music;

import vertbot.Main;
import vertbot.commands.Command;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class SilentCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Guild guild = event.getGuild();
		GuildSettings gs = Main.getGuildSettings(guild.getId());
		
		//setting prefix
		if(args.concat(0).toLowerCase().equals("on"))
		{
			String set = "on";
			
			gs.getJSON().put("silent", set);
			gs.save();
			
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Silent set");
			ebuilder.setDescription("Silent is now on");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		//clearing prefix
		else if(args.concat(0).toLowerCase().equals("off"))
		{
			gs.getJSON().put("silent", "off");
			gs.save();
			
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Silent set");
			ebuilder.setDescription("Silent is now off");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		else
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.addField("Silent is currently:", gs.getJSON().optString("silent", "off"), false);
			ebuilder.setColor(ColorUtil.neutral());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "silent";
	}
	
	@Override
	public String getUsage()
	{
		return "<on or off>";
	}
	
	@Override
	public String getDescription()
	{
		return "Turn silent mode on or off";
	}
	
	@Override
	public String getPermission()
	{
		return "";
	}
}
