package vertbot.commands.music;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;

public class ClearCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.getGuild().getSelfMember().getVoiceState().inVoiceChannel())
		{
			if(event.getGuild().getSelfMember().getVoiceState().getAudioChannel().getId().equals(event.getGuild().getMember(event.getAuthor()).getVoiceState().getAudioChannel().getId()))
			{
				Main.getPlayer(event.getGuild()).Stop();
				Main.getPlayer(event.getGuild()).Pause(false);
				Main.getPlayer(event.getGuild()).clear();
				event.getGuild().getAudioManager().closeAudioConnection();
				
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Success: Cleared playlist");
				ebuilder.setDescription("Cleared playlist. If you want to help keep me alive consider [donating on patreon!](https://www.patreon.com/vertbot)");
				ebuilder.setColor(ColorUtil.success());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
			else
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Not same voice channel");
				ebuilder.setDescription("You cannot do this unless you are in the same voice channel as me.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
		}
		else
		{
			Main.getPlayer(event.getGuild()).Stop();
			Main.getPlayer(event.getGuild()).Pause(false);
			Main.getPlayer(event.getGuild()).clear();
			
			event.getGuild().getAudioManager().closeAudioConnection();
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Cleared playlist");
			ebuilder.setDescription("Cleared playlist. If you want to help keep me alive consider [donating on patreon!](https://www.patreon.com/vertbot)");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "clear";
	}
	
	@Override
	public String getDescription()
	{
		return "Clears the current playlist";
	}
	
}
