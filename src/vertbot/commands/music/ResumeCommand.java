package vertbot.commands.music;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.music.MusicPlayer;
import vertbot.util.Args;
import vertbot.util.ColorUtil;

public class ResumeCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.getGuild().getSelfMember().getVoiceState().getAudioChannel().getId().equals(event.getGuild().getMember(event.getAuthor()).getVoiceState().getAudioChannel().getId()))
		{
			MusicPlayer player = Main.getPlayer(event.getGuild());
			player.Pause(false);
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Resuming playlist");
			ebuilder.setDescription("The playlist is being resumed.");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		else
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: Not same voice channel");
			ebuilder.setDescription("You cannot do this unless you are in the same voice channel as me.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "resume";
	}
	
	@Override
	public String getUsage()
	{
		return "";
	}
	
	@Override
	public String getDescription()
	{
		return "Resumes the playback";
	}
	
}
