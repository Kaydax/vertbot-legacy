package vertbot.commands.music;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;

public class RemoveCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			if(event.getGuild().getSelfMember().getVoiceState().getAudioChannel().getId().equals(event.getGuild().getMember(event.getAuthor()).getVoiceState().getAudioChannel().getId())) {
				Main.getPlayer(event.getGuild()).getPlaylist().removeTrack(Integer.parseInt(args.concat(0)) - 1);
				
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Success: Removed");
				ebuilder.setDescription(":recycle: Removed :recycle:");
				ebuilder.setColor(ColorUtil.success());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			} else {
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Not same voice channel");
				ebuilder.setDescription("You cannot do this unless you are in the same voice channel as me.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
		}
		catch(Exception e)
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: Invalid Number or playlist is empty");
			ebuilder.setDescription("Please put in a valid number or there is nothing to remove.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "remove";
	}
	
	@Override
	public String getUsage()
	{
		return "<number>";
	}
	
	@Override
	public String getDescription()
	{
		return "Removes a track from a playlist";
	}
	
}
