package vertbot.commands.music;


import org.json.JSONObject;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.music.MusicPlayer;
import vertbot.music.Playlist;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Processes;
import vertbot.util.Responses;
import vertbot.util.Utils;

public class AddCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			MusicPlayer player = Main.getPlayer(event.getGuild());
			if(!event.getGuild().getMember(event.getAuthor()).getVoiceState().inVoiceChannel())
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Not in voice channel");
				ebuilder.setDescription("You seem to not be in a voice channel or the same voice channel as me, so I can't do anything.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
			else
			{
				if(args.concat().length() > 0)
				{
					if(args.concat(0).contains("https://www.pornhub.com/view_video.php?") || args.concat(0).contains("http://www.pornhub.com/view_video.php?"))
					{
						EmbedBuilder ebuilder = new EmbedBuilder();
						ebuilder.setTitle("Error: Just why");
						ebuilder.setDescription("I don't even support it anyway, so why try to add it...");
						ebuilder.setColor(ColorUtil.error());
						MessageEmbed embed = ebuilder.build();
						MessageBuilder mbuilder = new MessageBuilder();
						mbuilder.setEmbed(embed);
						Message message = mbuilder.build();
						event.getChannel().sendMessage(message).complete();
					}
					if(args.concat(0).contains("https://") || args.concat(0).contains("http://"))
					{
						if(args.concat(0).contains("list="))
						{
							Responses.respond(event, "Please wait for me to check the playlist...");
							String input = args.concat(0).substring(args.concat(0).lastIndexOf("list=") + 5);
							String resp = Processes.node("playlist", Utils.quotify(input.split("&")[0]));
							try
							{
								if(resp.contains("error: Error:"))
								{
									EmbedBuilder ebuilder = new EmbedBuilder();
									ebuilder.setTitle("Error: Playlist");
									ebuilder.setDescription("An error has happend. This may be because the playlist is private or doesn't exist. Do the fix command for help if you think this is a bug.");
									ebuilder.setColor(ColorUtil.error());
									MessageEmbed embed = ebuilder.build();
									MessageBuilder mbuilder = new MessageBuilder();
									mbuilder.setEmbed(embed);
									Message message = mbuilder.build();
									event.getChannel().sendMessage(message).complete();
								}
								else
								{
									Playlist playlist = player.getPlaylist();
									playlist.setEvent(event);
									player.setEvent(event);
									GuildSettings gs = Main.getGuildSettings(event.getGuild().getId());
									Integer max = gs.getJSON().optInt("tier", Main.playlistSize);
									if(playlist.getList().size() < max) 
									{
										EmbedBuilder ebuilder = new EmbedBuilder();
										ebuilder.setTitle("Success: Added playlist");
										ebuilder.setDescription("Adding Playlist, please wait for it to load. Do the list command to see if it added everything. [Consider donating](https://www.patreon.com/vertbot) to up your " + max + " item limit");
										ebuilder.setColor(ColorUtil.success());
										MessageEmbed embed = ebuilder.build();
										MessageBuilder mbuilder = new MessageBuilder();
										mbuilder.setEmbed(embed);
										Message message = mbuilder.build();
										event.getChannel().sendMessage(message).complete();
										playlist.addTrack(args.concat(0));
									} else {
										EmbedBuilder ebuilder = new EmbedBuilder();
										ebuilder.setTitle("Error: Playlist full");
										ebuilder.setDescription("You cannot add anymore songs as your playlist is full. [Consider donating](https://www.patreon.com/vertbot)");
										ebuilder.setColor(ColorUtil.error());
										MessageEmbed embed = ebuilder.build();
										MessageBuilder mbuilder = new MessageBuilder();
										mbuilder.setEmbed(embed);
										Message message = mbuilder.build();
										event.getChannel().sendMessage(message).complete();
									}
								}
							}
							catch(Exception e)
							{
								EmbedBuilder ebuilder = new EmbedBuilder();
								ebuilder.setTitle("Error: Playlist");
								ebuilder.setDescription("An error has happend. This may be because the playlist is private or doesn't exist. Do the fix command for help if you think this is a bug.");
								ebuilder.setColor(ColorUtil.error());
								MessageEmbed embed = ebuilder.build();
								MessageBuilder mbuilder = new MessageBuilder();
								mbuilder.setEmbed(embed);
								Message message = mbuilder.build();
								event.getChannel().sendMessage(message).complete();
							}
						}
						else
						{
							Playlist playlist = player.getPlaylist();
							playlist.setEvent(event);
							player.setEvent(event);
							playlist.addTrack(args.concat(0));
						}
					}
					else
					{
						String resp = Processes.node("youtube", Utils.quotify(args.concat(0, -1, false)));
						JSONObject json = new JSONObject(resp);
						Playlist playlist = player.getPlaylist();
						playlist.setEvent(event);
						player.setEvent(event);
						if(json.getJSONArray("items").length() == 0 || json.getJSONArray("items").getJSONObject(0).getJSONObject("id").getString("kind").equals("youtube#channel"))
						{
							EmbedBuilder ebuilder = new EmbedBuilder();
							ebuilder.setTitle("Error: Search unclear");
							ebuilder.setDescription("Please make your search more clear as I could not find anything to play.");
							ebuilder.setColor(ColorUtil.error());
							MessageEmbed embed = ebuilder.build();
							MessageBuilder mbuilder = new MessageBuilder();
							mbuilder.setEmbed(embed);
							Message message = mbuilder.build();
							event.getChannel().sendMessage(message).complete();
						}
						else
						{
							JSONObject video = json.getJSONArray("items").getJSONObject(0);
							EmbedBuilder ebuilder = new EmbedBuilder();
							ebuilder.setTitle("" + video.getJSONObject("snippet").get("title"), "https://www.youtube.com/watch?v=" + video.getJSONObject("id").getString("videoId"));
							ebuilder.setDescription("By: " + video.getJSONObject("snippet").get("channelTitle"));
							ebuilder.setImage("" + video.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("high").get("url"));
							ebuilder.setColor(ColorUtil.neutral());
							MessageEmbed embed = ebuilder.build();
							MessageBuilder mbuilder = new MessageBuilder();
							mbuilder.setEmbed(embed);
							Message message = mbuilder.build();
							event.getChannel().sendMessage(message).complete();
							playlist.addTrack("https://www.youtube.com/watch?v=" + video.getJSONObject("id").getString("videoId"));
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			return;
		}
	}
	
	@Override
	public String getName()
	{
		return "add";
	}
	
	@Override
	public String getUsage()
	{
		return "<link or search>";
	}
	
	@Override
	public String getDescription()
	{
		return "Adds videos the playlist.";
	}
	
}
