package vertbot.commands.music;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.music.MusicPlayer;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.ColorUtil;

public class SkipCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			MusicPlayer player = Main.getPlayer(event.getGuild());
			GuildSettings gs = Main.getGuildSettings(event.getGuild().getId());
			if(event.getGuild().getSelfMember().getVoiceState().getAudioChannel().getId().equals(event.getGuild().getMember(event.getAuthor()).getVoiceState().getAudioChannel().getId()))
			{
				if(gs.getJSON().optString("repeat", "off").equals("on"))
				{
					EmbedBuilder ebuilder = new EmbedBuilder();
					ebuilder.setTitle("Error: Repeat is on");
					ebuilder.setDescription("Repeat is turned on, turn it off to skip.");
					ebuilder.setColor(ColorUtil.error());
					MessageEmbed embed = ebuilder.build();
					MessageBuilder mbuilder = new MessageBuilder();
					mbuilder.setEmbed(embed);
					Message message = mbuilder.build();
					event.getChannel().sendMessage(message).complete();
				}
				else
				{
					if(player.getPlaylist().getList().size() > 0)
					{
						player.setEvent(event);
						player.nextTrack();
						EmbedBuilder ebuilder = new EmbedBuilder();
						ebuilder.addField("Now Playing:", "`" + Integer.toString(player.getPlaylist().index + 1) + ": " + Main.getPlayer(event.getGuild()).getPlaying().title + "`", false);
						ebuilder.setColor(ColorUtil.neutral());
						MessageEmbed embed = ebuilder.build();
						MessageBuilder mbuilder = new MessageBuilder();
						mbuilder.setEmbed(embed);
						Message message = mbuilder.build();
						event.getChannel().sendMessage(message).complete();
					}
					else
					{
						EmbedBuilder ebuilder = new EmbedBuilder();
						ebuilder.setTitle("Error: Nothing to skip");
						ebuilder.setDescription("There is nothing to skip to.");
						ebuilder.setColor(ColorUtil.error());
						MessageEmbed embed = ebuilder.build();
						MessageBuilder mbuilder = new MessageBuilder();
						mbuilder.setEmbed(embed);
						Message message = mbuilder.build();
						event.getChannel().sendMessage(message).complete();
					}
				}
			}
		}
		catch(Exception e)
		{
			if(Main.getPlayer(event.getGuild()).getPlaylist().getList().size() == 1 && event.getGuild().getSelfMember().getVoiceState().inVoiceChannel())
			{
				return;
			}
			else
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Not same voice");
				ebuilder.setDescription("You cannot do this unless you are in the same voice channel as me.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
		}
		
	}
	
	@Override
	public String getName()
	{
		return "skip";
	}
	
	@Override
	public String getUsage()
	{
		return "";
	}
	
	@Override
	public String getDescription()
	{
		return "Skips the current playing track";
	}
	
}
