package vertbot.commands.music;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;

public class StopCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		try
		{
			String userv = event.getGuild().getMember(event.getAuthor()).getVoiceState().getAudioChannel().getId();
			String botv = event.getGuild().getMember(event.getJDA().getSelfUser()).getVoiceState().getAudioChannel().getId();
			if(userv.equals(botv))
			{
				event.getGuild().getAudioManager().closeAudioConnection();
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Success: Music Stopped");
				ebuilder.setDescription("Music stopped. If you want to help keep me alive consider [donating on patreon!](https://www.patreon.com/vertbot)");
				ebuilder.setColor(ColorUtil.success());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
			else
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Not same voice channel");
				ebuilder.setDescription("You cannot do this unless you are in the same voice channel as me.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
		}
		catch(Exception e)
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: Not same voice channel");
			ebuilder.setDescription("You cannot do this unless you are in the same voice channel as me.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "stop";
	}
	
	@Override
	public String getDescription()
	{
		return "Stops the current music playing";
	}
	
}
