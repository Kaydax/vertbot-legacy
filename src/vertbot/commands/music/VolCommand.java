package vertbot.commands.music;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;

public class VolCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(args.concat(0) == "")
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Current Volume:");
			ebuilder.setDescription(Main.getPlayer(event.getGuild()).getVolume());
			ebuilder.setColor(ColorUtil.neutral());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		else
		{
			try
			{
				Main.getPlayer(event.getGuild()).setVolume(Integer.parseInt(args.concat(0)));
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Success: Set volume");
				ebuilder.setDescription("Volume set to " + Main.getPlayer(event.getGuild()).getVolume());
				ebuilder.setColor(ColorUtil.success());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
			catch(Exception e)
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Invalid Number");
				ebuilder.setDescription("Please put in a valid number.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
		}
	}
	
	@Override
	public String getName()
	{
		return "volume";
	}
	
	@Override
	public String getDescription()
	{
		return "Sets the volume of the music";
	}
	
}
