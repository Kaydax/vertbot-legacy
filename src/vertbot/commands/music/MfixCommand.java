package vertbot.commands.music;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;

public class MfixCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.getGuild().getSelfMember().getVoiceState().inVoiceChannel())
		{
			if(event.getGuild().getSelfMember().getVoiceState().getAudioChannel().getId().equals(event.getGuild().getMember(event.getAuthor()).getVoiceState().getAudioChannel().getId()))
			{
				Main.getPlayer(event.getGuild()).Pause(false);
				Main.getPlayer(event.getGuild()).purge();
				event.getGuild().getAudioManager().closeAudioConnection();
				
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Success: Cleared music object");
				ebuilder.setDescription("I have tried and fix the music for you. You may have lost your playlist.");
				ebuilder.setColor(ColorUtil.success());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
			else
			{
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setTitle("Error: Not same voice channel");
				ebuilder.setDescription("You cannot do this unless you are in the same voice channel as me.");
				ebuilder.setColor(ColorUtil.error());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
		}
		else
		{
			Main.getPlayer(event.getGuild()).Pause(false);
			Main.getPlayer(event.getGuild()).purge();
			event.getGuild().getAudioManager().closeAudioConnection();
			
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Success: Cleared music object");
			ebuilder.setDescription("I have tried and fix the music for you. You may have lost your playlist.");
			ebuilder.setColor(ColorUtil.success());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "relmusic";
	}
	
	@Override
	public String getDescription()
	{
		return "Reloads the music for the current server to try and fix the bot from not playing.";
	}
	
}
