package vertbot.commands.fun;

import java.awt.Color;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Processes;
import vertbot.util.Utils;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class UrbanCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{		
		String resp = Processes.node("urban", Utils.quotify(args.concat(0, -1, false)));
		
		EmbedBuilder ebuilder = new EmbedBuilder();
		ebuilder.setTitle("Definition:", null);
		ebuilder.setColor(Color.GREEN);
		ebuilder.setDescription(Utils.clip(resp, 1900));
		MessageEmbed embed = ebuilder.build();
		MessageBuilder mbuilder = new MessageBuilder();
		mbuilder.setEmbed(embed);
		Message message = mbuilder.build();
		event.getChannel().sendMessage(message).complete();
	}
	
	@Override
	public String getName()
	{
		return "urban";
	}
	
	@Override
	public String getUsage()
	{
		return "<word>";
	}
	
	@Override
	public String getDescription()
	{
		return "Searches Urban for words you put in";
	}
	
	@Override
	public String getPermission()
	{
		return "";//Permissions.DEV;
	}
	
}
