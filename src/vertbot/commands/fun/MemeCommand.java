package vertbot.commands.fun;

import org.json.JSONObject;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Processes;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class MemeCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(args.concat().equals("dank")) {
			String resp = Processes.node("meme", "dank meme");
			JSONObject json = new JSONObject(resp);
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Requested by: " + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator(), null);
			ebuilder.addField("Url:", json.getJSONObject("data").getString("image_original_url"), true);
			ebuilder.setImage(json.getJSONObject("data").getString("image_original_url"));
			ebuilder.setFooter("Powered By Giphy", null);
			ebuilder.setColor(ColorUtil.randomColor());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		} else {
			String resp = Processes.node("meme", "meme");
			JSONObject json = new JSONObject(resp);
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Requested by: " + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator(), null);
			ebuilder.addField("Url:", json.getJSONObject("data").getString("image_original_url"), true);
			ebuilder.setImage(json.getJSONObject("data").getString("image_original_url"));
			ebuilder.setFooter("Powered By Giphy", null);
			ebuilder.setColor(ColorUtil.randomColor());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "meme";
	}
	
	@Override
	public String getUsage()
	{
		return "<dank or leave blank>";
	}
	
	@Override
	public String getDescription()
	{
		return "Gif memes, ya know.";
	}
	
}
