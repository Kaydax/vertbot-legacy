package vertbot.commands.fun;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class LMGTFYCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{	
		String[] arg = args.concat().split(" ");
		String resp = "http://lmgtfy.com/?q=" + String.join("+", arg);
		Responses.respond(event, resp);
	}
	
	@Override
	public String getName()
	{
		return "lmgtfy";
	}
	
	@Override
	public String getUsage()
	{
		return "<search>";
	}
	
	@Override
	public String getDescription()
	{
		return "Google stuff for your lazy friends";
	}
	
	@Override
	public String getPermission()
	{
		return "";//Permissions.DEV;
	}
	
}
