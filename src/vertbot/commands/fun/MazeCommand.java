package vertbot.commands.fun;

import java.io.File;

import javax.imageio.ImageIO;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.MazeGenerator;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;


public class MazeCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		int width = 32;
		int height = 32;
		
		try
		{
			if(args.size() >= 1)
			{
				width = Integer.parseInt(args.get(0));
			}
			if(args.size() >= 2)
			{
				height = Integer.parseInt(args.get(1));
			}
		}
		catch(NumberFormatException ex)
		{
			
		}
		
		width = Math.min(width, 256);
		height = Math.min(height, 256);
		
		try
		{
			int[][] maze = MazeGenerator.generate(width, height);
						
			ImageIO.write((args.size() >= 3 && args.get(2).toLowerCase().equals("color") ) ? MazeGenerator.drawMaze(maze) : MazeGenerator.drawMaze2(maze), "PNG", new File("temp.png"));
			event.getChannel().sendFile(new File("temp.png")).complete();
			//Responses.respond(event, "```\n" + str + "```");
			//System.out.println("maze sent");
		}
		catch(Exception ex)
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: Cannot make maze");
			ebuilder.setDescription("I couldn't make a maze.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
			return;
		}
		
		
	}
	
	@Override
	public String getName()
	{
		return "maze";
	}
	
	@Override
	public String getUsage()
	{
		return "<width> <height>";
	}
	
	@Override
	public String getDescription()
	{
		return "Generate a maze";
	}
	
}
