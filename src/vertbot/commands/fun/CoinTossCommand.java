package vertbot.commands.fun;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Processes;
import vertbot.util.Responses;
import vertbot.util.Utils;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class CoinTossCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{		
		String resp = Processes.node("coin", Utils.quotify(args.concat(0, -1, false)));
		
		Responses.respond(event, Utils.clip(resp, 1900));
	}
	
	@Override
	public String getName()
	{
		return "cointoss";
	}
	
	@Override
	public String getUsage()
	{
		return "";
	}
	
	@Override
	public String getDescription()
	{
		return "Tosses a coin to get heads or tails.";
	}
	
	@Override
	public String getPermission()
	{
		return "";//Permissions.DEV;
	}
	
}
