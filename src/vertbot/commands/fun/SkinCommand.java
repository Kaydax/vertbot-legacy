package vertbot.commands.fun;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Processes;
import vertbot.util.Responses;
import vertbot.util.Utils;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class SkinCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{		
		String resp = Processes.node("mcskin", Utils.quotify(args.concat(0, -1, false)));
		
		Responses.respond(event, Utils.clip(resp, 1900));
	}
	
	@Override
	public String getName()
	{
		return "skingrab";
	}
	
	@Override
	public String getUsage()
	{
		return "<minecraft username>";
	}
	
	@Override
	public String getDescription()
	{
		return "For all the kiddies who want this.";
	}
	
	@Override
	public String getPermission()
	{
		return "";//Permissions.DEV;
	}
	
}
