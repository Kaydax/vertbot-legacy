package vertbot.commands.fun;

import org.apache.commons.codec.binary.Base64;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class Base64Command extends Command
{
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		byte[] b64 = args.concat().getBytes();
		Responses.respond(event, "```" + Base64.encodeBase64String(b64) + "```");
	}
	
	@Override
	public String getName()
	{
		return "b64";
	}
	
	@Override
	public String getUsage()
	{
		return "<text>";
	}
	
	@Override
	public String getDescription()
	{
		return "Makes text turn into base64!";
	}
	
}
