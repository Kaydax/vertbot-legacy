package vertbot.commands.fun;

import org.json.JSONObject;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Processes;
import vertbot.util.Responses;
import vertbot.util.Utils;

public class GelbooruCommand extends Command
{
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		TextChannel channel = event.getJDA().getGuildById(event.getGuild().getId()).getTextChannelById(event.getChannel().getId());
		if(channel.isNSFW())
		{
			String resp = Processes.node("gelbooru", Utils.quotify(args.concat(0, -1, false)));
			
			try
			{
				JSONObject json = new JSONObject(resp);
				
				String url = "";
				String img = "";
				String tags = "";
				
				if(json.getString("tags").contains("loli")) {
					url = "https://discordapp.com/guidelines";
					img = null;
					tags = "I cannot display anything containing loli as it breaks the discord guidelines. Sorry about this.";
				} else {
					url = json.getString("url");
					img = json.getString("url");
					tags = "```" + json.getString("tags").trim().replace(" ", ", ") + "```";
				}
				
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.setImage(img);
				ebuilder.setTitle("Requested by: " + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator(), null);
				ebuilder.addField("Tags:", tags, false);
				if(json.getString("url").contains("webm"))
				{
					ebuilder.addField("Video URL:", url, false);
				}
				else
				{
					ebuilder.addField("Image URL:", url, false);
				}
				ebuilder.setColor(ColorUtil.randomColor());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				event.getChannel().sendMessage(message).complete();
			}
			catch(Exception e)
			{
				Responses.respond(event, "No results found for the tag(s): `" + args.concat().trim().replace(" ", ", ") + "`");
				//StringWriter sw = new StringWriter();
				//e.printStackTrace(new PrintWriter(sw));
				//String es = sw.toString();
				//event.getChannel().sendMessage(event.getAuthor().getAsMention() + es).complete();
			}
		}
		else
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: The channel is not nsfw");
			ebuilder.setDescription("The channel must be nsfw in order to use this.");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}
	
	@Override
	public String getName()
	{
		return "gelbooru";
	}
	
	@Override
	public String getUsage()
	{
		return "<tag>";
	}
	
	@Override
	public String getDescription()
	{
		return "Gelbooru! *Split tags with a space*";
	}
	
	@Override
	public String getPermission()
	{
		return "";//Permissions.DEV;
	}
	
}
