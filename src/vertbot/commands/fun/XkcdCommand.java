package vertbot.commands.fun;

import java.io.BufferedReader;
import java.io.FileReader;

import org.json.JSONObject;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.commands.Command;
import vertbot.util.Args;

public class XkcdCommand extends Command
{
	private static BufferedReader br;

	public static String readFile(String filename) {
		String result = "";
	    try {
	        br = new BufferedReader(new FileReader(filename));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        result = sb.toString();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return result;
    }
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		String jsonData = readFile("node/xkcd/cache.json");
		System.out.println(jsonData);
	    JSONObject json = new JSONObject(jsonData);
	    System.out.println(json.getString("Archive"));
	}

	@Override
	public String getName()
	{
		return "xkcd";
	}

	@Override
	public String getUsage()
	{
		return "<id>";
	}

	@Override
	public String getDescription()
	{
		return "Xkcd";
	}

	@Override
	public String getPermission()
	{
		return "";// Permissions.DEV;
	}

}
