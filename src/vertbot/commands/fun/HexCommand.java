package vertbot.commands.fun;

import org.apache.commons.codec.binary.Hex;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class HexCommand extends Command
{
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		byte[] hex = args.concat().getBytes();
		Responses.respond(event, "```" + Hex.encodeHexString(hex).replaceAll("..", "$0 ") + "```");
	}
	
	@Override
	public String getName()
	{
		return "hex";
	}
	
	@Override
	public String getUsage()
	{
		return "<text>";
	}
	
	@Override
	public String getDescription()
	{
		return "Makes text turn into hex!";
	}
	
}
