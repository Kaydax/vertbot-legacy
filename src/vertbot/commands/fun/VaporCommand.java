package vertbot.commands.fun;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class VaporCommand extends Command
{
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		Responses.respond(event, "```" + args.concat().replaceAll(".", "$0 ") + "```");
	}
	
	@Override
	public String getName()
	{
		return "vapor";
	}
	
	@Override
	public String getUsage()
	{
		return "<text>";
	}
	
	@Override
	public String getDescription()
	{
		return "A e s t h e t i c";
	}
	
}
