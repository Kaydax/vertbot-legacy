package vertbot.commands.fun;

import org.apache.commons.codec.digest.DigestUtils;

import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class MD5Command extends Command
{
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		byte[] hash = args.concat().getBytes();
		Responses.respond(event, "```" + DigestUtils.md5Hex(hash) + "```");
	}
	
	@Override
	public String getName()
	{
		return "md5";
	}
	
	@Override
	public String getUsage()
	{
		return "<text>";
	}
	
	@Override
	public String getDescription()
	{
		return "Makes text turn into md5 hash!";
	}
	
}
