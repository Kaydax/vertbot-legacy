package vertbot.commands.fun;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.commands.Command;
import vertbot.util.Args;
import vertbot.util.ColorUtil;
import vertbot.util.Processes;
import vertbot.util.Utils;

public class ComicCommand extends Command
{
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{		
		String resp = Processes.node("comic", Utils.quotify(args.concat(0, -1, false)));
		
		EmbedBuilder ebuilder = new EmbedBuilder();
		ebuilder.setImage(Utils.clip(resp, 1900));
		ebuilder.setFooter("Command made with the help from ROM Typo", null);
		ebuilder.setColor(ColorUtil.randomColor());
		MessageEmbed embed = ebuilder.build();
		MessageBuilder mbuilder = new MessageBuilder();
		mbuilder.setEmbed(embed);
		Message message = mbuilder.build();
		event.getChannel().sendMessage(message).complete();
	}
	
	@Override
	public String getName()
	{
		return "comic";
	}
	
	@Override
	public String getUsage()
	{
		return "";
	}
	
	@Override
	public String getDescription()
	{
		return "Post a random comic";
	}
	
	@Override
	public String getPermission()
	{
		return "";//Permissions.DEV;
	}
	
}
