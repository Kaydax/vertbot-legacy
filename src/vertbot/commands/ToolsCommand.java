package vertbot.commands;

import java.util.HashMap;

import vertbot.commands.tools.GuildPicCommand;
import vertbot.commands.tools.IDCommand;
import vertbot.commands.tools.JSCommand;
import vertbot.commands.tools.PicCommand;
import vertbot.commands.tools.PingCommand;
import vertbot.commands.tools.WhoIsCommand;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class ToolsCommand extends Command
{
	public ToolsCommand()
	{
		super();
		
		this.addCommand(new InfoCommand());
		this.addCommand(new IDCommand());
		this.addCommand(new PicCommand());
		this.addCommand(new WhoIsCommand());
		this.addCommand(new GuildPicCommand());
		this.addCommand(new JSCommand());
		this.addCommand(new PingCommand());
	}
	
	static HashMap<String, GuildSettings> guildSettingsCache = new HashMap<>();
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.isFromType(ChannelType.PRIVATE))
		{
			Responses.noPrivate(event);
			
			return;
		}
		
		if(args.size() == 0)
		{
			Responses.respond(event, "```" + new HelpCommand().getHelp(event, this) + "```");
			return;
		}
		
		super.doCommand(event, args);
		
		/*if(args.concat().length() == 0) {
			GuildSettings gs = getGuildSettings(event.getGuild().getId());
			Responses.respond(event, "This command has sub-commands. Please do '" + gs.getJSON().optString("prefix", "") + "help tools' to see the sub-commands");
		}*/
	}

	@Override
	public String getName()
	{
		return "tools";
	}
	
	@Override
	public String getDescription()
	{
		return "Tool Commands <Sub-menu>";
	}
	
	@Override
	public String getPermission()
	{
		return "";
	}
	
}
