package vertbot.commands;

import java.util.HashMap;

import vertbot.commands.music.AddCommand;
import vertbot.commands.music.ClearCommand;
import vertbot.commands.music.GotoCommand;
import vertbot.commands.music.MfixCommand;
import vertbot.commands.music.PauseCommand;
import vertbot.commands.music.PlayCommand;
import vertbot.commands.music.PlaylistCommand;
import vertbot.commands.music.RemoveCommand;
import vertbot.commands.music.RepeatCommand;
import vertbot.commands.music.ResumeCommand;
import vertbot.commands.music.ShuffleCommand;
import vertbot.commands.music.SilentCommand;
import vertbot.commands.music.SkipCommand;
import vertbot.commands.music.StopCommand;
import vertbot.commands.music.VolCommand;
import vertbot.settings.GuildSettings;
import vertbot.util.Args;
import vertbot.util.Responses;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class MusicCommand extends Command
{
	public MusicCommand()
	{
		super();
		
		this.addCommand(new AddCommand());
		this.addCommand(new PlayCommand());
		this.addCommand(new StopCommand());
		this.addCommand(new ClearCommand());
		this.addCommand(new PauseCommand());
		this.addCommand(new ResumeCommand());
		this.addCommand(new ShuffleCommand());
		this.addCommand(new RepeatCommand());
		this.addCommand(new SkipCommand());
		this.addCommand(new PlaylistCommand());
		this.addCommand(new VolCommand());
		this.addCommand(new RemoveCommand());
		this.addCommand(new GotoCommand());
		this.addCommand(new SilentCommand());
		this.addCommand(new MfixCommand());
	}
	
	static HashMap<String, GuildSettings> guildSettingsCache = new HashMap<>();
	
	@Override
	public void doCommand(MessageReceivedEvent event, Args args)
	{
		if(event.isFromType(ChannelType.PRIVATE))
		{
			Responses.noPrivate(event);
			
			return;
		}
		
		if(args.size() == 0)
		{
			Responses.respond(event, "```" + new HelpCommand().getHelp(event, this) + "```");
			return;
		}
		
		super.doCommand(event, args);
		
		/*if(args.concat().length() == 0) {
			GuildSettings gs = getGuildSettings(event.getGuild().getId());
			Responses.respond(event, "This command has sub-commands. Please do '" + gs.getJSON().optString("prefix", "") + "help tools' to see the sub-commands");
		}*/
	}
	
	@Override
	public String getName()
	{
		return "music";
	}
	
	@Override
	public String getDescription()
	{
		return "Music Commands <Sub-menu>";
	}
	
	@Override
	public String getPermission()
	{
		return "";
	}
	
}
