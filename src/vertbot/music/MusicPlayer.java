package vertbot.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;

import vertbot.Main;
import vertbot.settings.GuildSettings;
import vertbot.util.Apsh;
import vertbot.util.ColorUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class MusicPlayer
{
	Playlist playlist = new Playlist();

	AudioPlayer player;
	
	MessageReceivedEvent event;
	
	public MusicPlayer(Guild guild)
	{
		this.guild = guild;
		Main.apm.source(YoutubeAudioSourceManager.class).setPlaylistPageCount(Integer.MAX_VALUE);
		Main.apm.setPlayerCleanupThreshold(30000);
		Main.apm.getConfiguration().setOpusEncodingQuality(10);
		Main.apm.setTrackStuckThreshold(1000);
		//guild.getAudioManager().setConnectionListener(new ConListener());
		player = Main.apm.createPlayer();
		guild.getAudioManager().setSendingHandler(new Apsh(this.player));
		player.addListener(new TrackScheduler(this));
	}
	
	Guild guild;
	
	public void Play()
	{
		if(playlist.index == playlist.getList().size())
		{
			(new Thread() {public void run() {guild.getAudioManager().closeAudioConnection();}}).start();
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setDescription("Finished Playlist. If you want to help keep me alive consider [donating on patreon!](https://www.patreon.com/vertbot)");
			ebuilder.setColor(ColorUtil.neutral());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
		if(playlist.getList().size() > 0)
		{
			AudioTrack track = playlist.getCurrent();
			player.playTrack(track.makeClone());
		}
	}
	
	public void Stop()
	{
		player.stopTrack();
	}
	
	public void Pause(boolean value)
	{
		player.setPaused(value);
	}
	
	/*------------------------------------------------------------*/
	
	public void nextTrack()
	{
		GuildSettings gs = Main.getGuildSettings(guild.getId());
		this.Stop();
		if(playlist.index < playlist.tracks.size())
		{
			if(gs.getJSON().optString("shuffle", "off").equals("on"))
			{
				playlist.getRandom();
				this.Play();
			}
			else if(gs.getJSON().optString("repeat", "off").equals("on"))
			{
				this.Play();
			}
			else
			{
				playlist.index++;
				this.Play();
			}
		}
	}
	
	/*------------------------------------------------------------*/
	
	public MessageReceivedEvent getEvent()
	{
		return this.event;
	}
	
	public void setEvent(MessageReceivedEvent event)
	{
		this.event = event;
	}
	
	/*------------------------------------------------------------*/
	
	public void setVolume(int input)
	{
		player.setVolume(input);
	}
	
	/*------------------------------------------------------------*/
	
	public void clear()
	{
		playlist.clear();
	}
	
	public void purge()
	{
		player.destroy();
	}
	
	/*------------------------------------------------------------*/
	
	public AudioTrackInfo getPlaying()
	{
		return player.getPlayingTrack().getInfo();
	}
	
	public AudioTrack getTrack()
	{
		AudioTrack track = playlist.getCurrent();
		return track;
	}
	
	public AudioPlayer getPlayer()
	{
		return player;
	}
	
	public String getVolume()
	{
		return new Integer(player.getVolume()).toString() + "%";
	}
	
	public Playlist getPlaylist()
	{
		return playlist;
	}
	
	public boolean isPlaying() {
        return player.getPlayingTrack() != null;
    }
}
