package vertbot.music;

import vertbot.Main;
import vertbot.settings.GuildSettings;
import vertbot.util.ColorUtil;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;

public class TrackScheduler extends AudioEventAdapter
{
	
	MusicPlayer player;
	
	public TrackScheduler(MusicPlayer player)
	{
		this.player = player;
	}
	
	@Override
	public void onPlayerPause(AudioPlayer player)
	{
		// Player was paused
	}
	
	@Override
	public void onPlayerResume(AudioPlayer player)
	{
		// Player was resumed
	}
	
	@Override
	public void onTrackStart(AudioPlayer player, AudioTrack track)
	{
		// A track started playing
	}
	
	@Override
	public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason)
	{
		if(endReason == AudioTrackEndReason.FINISHED)
		{
			GuildSettings gs = Main.getGuildSettings(this.player.event.getGuild().getId());
			this.player.nextTrack();
			if(gs.getJSON().optString("silent", "off").equals("on")) {
				return;
			} else {
				EmbedBuilder ebuilder = new EmbedBuilder();
				ebuilder.addField("Now Playing:", "`" + Integer.toString(this.player.getPlaylist().index + 1) + ": " + Main.getPlayer(this.player.event.getGuild()).getPlaying().title + "`", false);
				ebuilder.setColor(ColorUtil.neutral());
				MessageEmbed embed = ebuilder.build();
				MessageBuilder mbuilder = new MessageBuilder();
				mbuilder.setEmbed(embed);
				Message message = mbuilder.build();
				this.player.event.getChannel().sendMessage(message).complete();
			}
		}
		
		// endReason == FINISHED: A track finished or died by an exception (mayStartNext = true).
		// endReason == LOAD_FAILED: Loading of a track failed (mayStartNext = true).
		// endReason == STOPPED: The player was stopped.
		// endReason == REPLACED: Another track started playing while this had not finished
		// endReason == CLEANUP: Player hasn't been queried for a while, if you want you can put a
		//                       clone of this back to your queue
	}
	
	@Override
	public void onTrackException(AudioPlayer player, AudioTrack track, FriendlyException exception)
	{
		this.player.Play();
	}
	
	@Override
	public void onTrackStuck(AudioPlayer player, AudioTrack track, long thresholdMs)
	{
		this.player.Play();
	}
}
