package vertbot.music;

import java.util.ArrayList;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import vertbot.Main;
import vertbot.settings.GuildSettings;
import vertbot.util.ColorUtil;

public class Playlist
{
	MessageReceivedEvent event;
	ArrayList<AudioTrack> tracks;

	public int index;

	public Playlist()
	{
		this.tracks = new ArrayList<AudioTrack>();
	}

	public void setCurrent(int index)
	{
		this.index = index;
	}

	public AudioTrack getLast()
	{
		return this.tracks.get(this.tracks.size() - 1);
	}

	public void getRandom()
	{
		index = (int) (Math.random() * tracks.size());
	}

	public AudioTrack getNext()
	{
		return this.tracks.remove(0);
	}

	public AudioTrack getCurrent()
	{
		return this.tracks.get(index);
	}

	public AudioTrack getList(int index)
	{
		return this.tracks.get(index);
	}

	public ArrayList<AudioTrack> getList()
	{
		return this.tracks;
	}

	public AudioTrack removeTrack(int index)
	{
		return this.tracks.remove(index);
	}

	public void addTrack(AudioTrack track)
	{
		GuildSettings gs = Main.getGuildSettings(event.getGuild().getId());
		Integer max = gs.getJSON().optInt("tier", Main.playlistSize);
		if (getList().size() < max)
		{
			this.tracks.add(track);
		}
	}

	public void addTrack(String url)
	{
		GuildSettings gs = Main.getGuildSettings(event.getGuild().getId());
		final Integer max = gs.getJSON().optInt("tier", Main.playlistSize);
		if (getList().size() < max)
		{
			Main.apm.loadItem(url, new AudioLoadResultHandler()
			{
				@Override
				public void trackLoaded(AudioTrack track)
				{
					MusicPlayer player = Main.getPlayer(event.getGuild());
					Playlist pl = player.getPlaylist();
					pl.addTrack(track);

					if (player.getEvent() != null)
					{
						EmbedBuilder ebuilder = new EmbedBuilder();
						ebuilder.setTitle("Success: Added track");
						ebuilder.setDescription("Added **" + track.getInfo().title + "** to the playlist. Use the play command to start the playlist. [Consider donating](https://www.patreon.com/vertbot) to up your " + max + " item limit");
						ebuilder.setColor(ColorUtil.success());
						MessageEmbed embed = ebuilder.build();
						MessageBuilder mbuilder = new MessageBuilder();
						mbuilder.setEmbed(embed);
						Message message = mbuilder.build();
						event.getChannel().sendMessage(message).complete();
					}
				}

				@Override
				public void playlistLoaded(AudioPlaylist playlist)
				{
					for (AudioTrack track : playlist.getTracks())
					{
						addTrack(track);
					}
				}

				@Override
				public void noMatches()
				{
					// Notify the user that we've got nothing
				}

				@Override
				public void loadFailed(FriendlyException throwable)
				{
					// Notify the user that everything exploded
				}
			});
		} else {
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Error: Playlist full");
			ebuilder.setDescription("You cannot add anymore songs as your playlist is full. [Consider donating](https://www.patreon.com/vertbot)");
			ebuilder.setColor(ColorUtil.error());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			event.getChannel().sendMessage(message).complete();
		}
	}

	public void clear()
	{
		this.tracks.clear();
	}

	public MessageReceivedEvent getEvent()
	{
		return this.event;
	}

	public void setEvent(MessageReceivedEvent event)
	{
		this.event = event;
	}
}
