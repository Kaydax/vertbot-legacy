package vertbot.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Processes
{
	/** launches the given script in nodejs with the given args; returns the output (null if failed);*/
	public static String node(String script, String... args)
	{
		List<String> argList = new ArrayList<String>();
		argList.addAll(Arrays.asList(new String[]
		{ "node", script })); //add node command and script file
		argList.addAll(Arrays.asList(args)); //add the rest of the args
		
		try
		{
			ProcessBuilder ps = new ProcessBuilder(argList);
			ps.directory(new File("node")); //node stuff should all be in its own folder called "node"
			ps.redirectErrorStream(true);
			Process pr = ps.start();
			BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream(), "UTF8"));
			String line;
			String output = "";
			while((line = in.readLine()) != null)
			{
				output += line + "\n";
			}
			pr.waitFor();
			
			return output;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return null;
	}
}
