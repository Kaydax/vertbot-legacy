package vertbot.util;

import javax.security.auth.login.LoginException;

import com.sedmelluq.discord.lavaplayer.jdaudp.NativeAudioSendFactory;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import vertbot.Main;

public class Shard
{
	public JDA jda;

    public Shard(int shardId, int totalShards, String token) {
        System.out.println("[" + (shardId + 1) + "/" + totalShards + "] Logging in...");
        try {
            this.jda = new JDABuilder(AccountType.BOT)
                    .setToken(token)
                    .setAudioSendFactory(new NativeAudioSendFactory())
                    .addEventListener(new Main())
                    .useSharding(shardId, totalShards)
                    .buildAsync();
        } catch (LoginException ignored) {
        	System.out.println("[" + (shardId + 1) + "/" + totalShards + "] Failed to login");
        }
    }
}
