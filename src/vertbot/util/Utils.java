package vertbot.util;

import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.utils.PermissionUtil;
import vertbot.commands.Command;

public class Utils
{
	public Command findCommand(Command root, String command)
	{
		return null;
	}
	
	public static int strMatch(String a, String b)
	{
		int size = Math.min(a.length(), b.length());
		
		for(int i = 0; i < size; i++)
		{
			if(a.charAt(i) != b.charAt(i))
			{
				return i - 1;
			}
		}
		
		return size;
	}
	
	public static String durationString(int secondtTime)
	{
		TimeZone tz = TimeZone.getTimeZone("UTC");
		SimpleDateFormat df = new SimpleDateFormat("m:ss");
		df.setTimeZone(tz);
		String time = df.format(new Date(secondtTime * 1000L));
		
		return time;
		
	}
	
	/** returns the best matching permission for the given name or null if not found */
	public static Permission findPermission(String name)
	{
		name = name.trim().toLowerCase();
		
		Permission[] perms = Permission.values();
		
		int matchLen = 0;
		Permission match = null;
		
		for(int i = 0; i < perms.length; i++)
		{
			Permission perm = perms[i];
			
			int len = strMatch(perm.name().trim().toLowerCase(), name);
			
			if(len > matchLen)
			{
				match = perm;
			}
		}
		
		return match;
	}
	
	/** returns the best-fit role for the given name, or null if none found */
	public static Role findRole(Guild guild, String name)
	{
		name = name.trim().toLowerCase();
		
		List<Role> roles = guild.getRoles();
		
		int matchLen = 0;
		Role match = null;
		
		for(Iterator<Role> iter = roles.iterator(); iter.hasNext();)
		{
			Role role = iter.next();
			
			int len = strMatch(role.getName().trim().toLowerCase(), name);
			
			if(len > matchLen)
			{
				match = role;
			}
		}
		
		return match;
	}
	
	/** returns a list of guilds that the given user is a member of */
	public static List<Guild> findGuildsByUser(JDA jda, User user)
	{
		List<Guild> guilds = jda.getGuilds();
		List<Guild> retGuilds = new ArrayList<Guild>();
		
		for(Iterator<Guild> iter = guilds.iterator(); iter.hasNext();)
		{
			Guild guild = iter.next();
			
			if(guild.isMember(user))
			{
				retGuilds.add(guild);
			}
		}
		
		return retGuilds;
	}
	
	public static Guild findGuildByName(JDA jda, String name)
	{
		name = name.trim().toLowerCase();
		
		List<Guild> guilds = jda.getGuilds();
		
		int matchLen = -1;
		Guild match = null;
		
		for(Iterator<Guild> iter = guilds.iterator(); iter.hasNext();)
		{
			Guild guild = iter.next();
			
			int newMatchLen = strMatch(guild.getName().trim().toLowerCase(), name);
			
			if(newMatchLen > matchLen)
			{
				matchLen = newMatchLen;
				match = guild;
			}
		}
		
		return match;
	}
	
	public static Member findUserByName(JDA jda, String name)
	{
		return findUserByName(jda, null, name);
	}
	
	/** rename to findMemberByName? */
	public static Member findUserByName(JDA jda, Guild guild, String name)
	{
		List<Member> users = null;
		
		if(guild != null)
		{
			users = guild.getMembers();
		}
		else
		{
			users = getAllUsers(jda);
		}
		
		int matchLen = -1;
		Member match = null;
		
		for(Iterator<Member> iter = users.iterator(); iter.hasNext();)
		{
			Member user = iter.next();
			
			int newMatchLen = strMatch(user.getUser().getName().trim().toLowerCase(), name);
			
			if(newMatchLen > matchLen)
			{
				matchLen = newMatchLen;
				match = user;
			}
		}
		
		return match;
	}
	
	public static TextChannel findTextChannelByName(JDA jda, String name)
	{
		return findTextChannelByName(jda, null, name);
	}
	
	public static TextChannel findTextChannelByName(JDA jda, Guild guild, String name)
	{
		List<TextChannel> chans = null;
		
		if(guild != null)
		{
			chans = guild.getTextChannels();
		}
		else
		{
			chans = getAllTextChannels(jda);
		}
		
		int matchLen = -1;
		TextChannel match = null;
		
		for(Iterator<TextChannel> iter = chans.iterator(); iter.hasNext();)
		{
			TextChannel chan = iter.next();
			
			int newMatchLen = strMatch(chan.getName().trim().toLowerCase(), name);
			
			if(newMatchLen > matchLen)
			{
				matchLen = newMatchLen;
				match = chan;
			}
		}
		
		return match;
	}
	
	public static VoiceChannel findVoiceChannelByName(JDA jda, String name)
	{
		return findVoiceChannelByName(jda, null, name);
	}
	
	public static VoiceChannel findVoiceChannelByName(JDA jda, Guild guild, String name)
	{
		List<VoiceChannel> chans = null;
		
		if(guild != null)
		{
			chans = guild.getVoiceChannels();
		}
		else
		{
			chans = getAllVoiceChannels(jda);
		}
		
		int matchLen = -1;
		VoiceChannel match = null;
		
		for(Iterator<VoiceChannel> iter = chans.iterator(); iter.hasNext();)
		{
			VoiceChannel chan = iter.next();
			
			int newMatchLen = strMatch(chan.getName().trim().toLowerCase(), name);
			
			if(newMatchLen > matchLen)
			{
				matchLen = newMatchLen;
				match = chan;
			}
		}
		
		return match;
	}
	
	//public static List<TextChannel> get all
	
	/** returns all users(members currently) the bot knows about */
	public static List<Member> getAllUsers(JDA jda)
	{
		HashSet<Member> users = new HashSet<Member>();
		
		List<Guild> guilds = jda.getGuilds();
		
		for(Iterator<Guild> gIter = guilds.iterator(); gIter.hasNext();)
		{
			Guild guild = gIter.next();
			List<Member> guildUsers = guild.getMembers();
			
			for(Iterator<Member> uIter = guildUsers.iterator(); uIter.hasNext();)
			{
				Member user = uIter.next();
				
				users.add(user);
			}
		}
		
		return new ArrayList<Member>(users);
	}
	
	/** returns all text channels the bot knows about */
	public static List<TextChannel> getAllTextChannels(JDA jda)
	{
		HashSet<TextChannel> chans = new HashSet<TextChannel>();
		
		List<Guild> guilds = jda.getGuilds();
		
		for(Iterator<Guild> gIter = guilds.iterator(); gIter.hasNext();)
		{
			Guild guild = gIter.next();
			List<TextChannel> guildChans = guild.getTextChannels();
			
			for(Iterator<TextChannel> cIter = guildChans.iterator(); cIter.hasNext();)
			{
				TextChannel chan = cIter.next();
				
				chans.add(chan);
			}
		}
		
		return new ArrayList<TextChannel>(chans);
	}
	
	/** returns all text channels the bot knows about */
	public static List<VoiceChannel> getAllVoiceChannels(JDA jda)
	{
		HashSet<VoiceChannel> chans = new HashSet<VoiceChannel>();
		
		List<Guild> guilds = jda.getGuilds();
		
		for(Iterator<Guild> gIter = guilds.iterator(); gIter.hasNext();)
		{
			Guild guild = gIter.next();
			List<VoiceChannel> guildChans = guild.getVoiceChannels();
			
			for(Iterator<VoiceChannel> cIter = guildChans.iterator(); cIter.hasNext();)
			{
				VoiceChannel chan = cIter.next();
				
				chans.add(chan);
			}
		}
		
		return new ArrayList<VoiceChannel>(chans);
	}
	
	public static String debugOut(MessageReceivedEvent event, boolean showIDs)
	{
		String str = "";
		
		if(!event.isFromType(ChannelType.PRIVATE))
		{
			Guild guild = event.getGuild();
			TextChannel chan = (TextChannel) event.getChannel();
			
			str += guild.getName();
			str += showIDs ? "[" + guild.getId() + "]" : "";
			str += "/";
			str += chan.getName();
			str += showIDs ? "[" + chan.getId() + "]" : "";
			str += "/";
		}
		
		str += event.getAuthor().getName();
		str += showIDs ? "[" + event.getAuthor().getId() + "]" : "";
		str += ": " + event.getMessage().getContentDisplay();
		
		return str;
	}
	
	public static TextChannel getLogicalChannel(Guild guild)
	{
		List<TextChannel> channels = guild.getTextChannels();
		
		TextChannel first, general, bot;
		first = general = bot = null;
		
		if(channels.size() == 0)
		{
			return null;
		}
		
		for(int i = 0; i < channels.size(); i++)
		{
			TextChannel chan = channels.get(i);
			
			if(canSpeak(chan))
			{
				if(first == null)
				{
					first = chan;
				}
				if(general == null && chan.getName().toLowerCase().contains("general"))
				{
					general = chan;
				}
				if(bot == null && chan.getName().toLowerCase().contains("bot"))
				{
					bot = chan;
				}
			}
		}
		
		return bot != null ? bot : general != null ? general : first != null ? first : null;
	}
	
	public static boolean canSpeak(TextChannel chan)
	{
		return PermissionUtil.checkPermission(chan, chan.getGuild().getMember(chan.getJDA().getSelfUser()), Permission.MESSAGE_WRITE);
	}
	
	public static Dimension getScaledDimension(Dimension imgSize, Dimension boundary)
	{
		
		int original_width = imgSize.width;
		int original_height = imgSize.height;
		int bound_width = boundary.width;
		int bound_height = boundary.height;
		int new_width = original_width;
		int new_height = original_height;
		
		// first check if we need to scale width
		if(original_width > bound_width)
		{
			//scale width to fit
			new_width = bound_width;
			//scale height to maintain aspect ratio
			new_height = (new_width * original_height) / original_width;
		}
		
		// then check if we need to scale even with the new height
		if(new_height > bound_height)
		{
			//scale height to fit instead
			new_height = bound_height;
			//scale width to maintain aspect ratio
			new_width = (new_height * original_width) / original_height;
		}
		
		return new Dimension(new_width, new_height);
	}
	
	/** clips a string to the given max length */
	public static String clip(String str, int length)
	{
		if(str == null || str.length() == 0)
		{
			return str;
		}
		
		return str.substring(0, Math.min(str.length(), length));
	}
	
	/** surrounds the string in quotes, adds a level of escaping to preexisting quotes */
	public static String quotify(String str)
	{
		return str;
	}
	
	public static Member getUserByName(MessageReceivedEvent event, String name)
	{
		List<Member> users = event.getGuild().getMembers();
		name = name.toLowerCase();
		
		//find by earliest index:
		int lowestIndex = 9999;
		Member foundUser = null;
		
		for(Iterator<Member> iter = users.iterator(); iter.hasNext();)
		{
			Member user = iter.next();
			int find = user.getUser().getName().toLowerCase().indexOf(name);
			
			if(find != -1 && find < lowestIndex)
			{
				lowestIndex = find;
				foundUser = user;
			}
		}
		
		return foundUser;
	}
	
	/** returns a set of permissions the speaker has */
	public static TreeSet<Permission> getPerms(MessageReceivedEvent event, Member user)
	{
		List<Role> roles = user.getRoles();
		
		TreeSet<Permission> perms = new TreeSet<>(new PermissionComparator());
		for(int i = 0; i < roles.size(); i++)
		{
			List<Permission> rolePerms = roles.get(i).getPermissions();
			for(int j = 0; j < rolePerms.size(); j++)
			{
				perms.add(rolePerms.get(j));
			}
		}
		
		return perms;
	}
	
	/** computation time of this function goes up as number of guilds+users increases... */
	public static int getUniqueUsers(MessageReceivedEvent event)
	{
		JDA jda = event.getJDA();
		
		Set<String> uniqueUserIDs = new HashSet<String>();
		
		for(Iterator<Guild> guilds = jda.getGuilds().iterator(); guilds.hasNext();)
		{
			Guild guild = guilds.next();
			
			for(Iterator<Member> users = guild.getMembers().iterator(); users.hasNext();)
			{
				Member user = users.next();
				
				uniqueUserIDs.add(user.getUser().getId());
			}
		}
		
		return uniqueUserIDs.size();
	}
	
	private static class PermissionComparator implements Comparator<Permission>
	{
		@Override
		public int compare(Permission a, Permission b)
		{
			return a.name().compareTo(b.name());
		}
	}
	
	/** ripped from apache ant */
	static final String[] translateCommandline(final String toProcess)
	{
		if(toProcess == null || toProcess.length() == 0)
		{
			// no command? no string
			return new String[0];
		}
		
		// parse with a simple finite state machine
		
		final int normal = 0;
		final int inQuote = 1;
		final int inDoubleQuote = 2;
		int state = normal;
		final StringTokenizer tok = new StringTokenizer(toProcess, "\"\' ", true);
		final ArrayList<String> list = new ArrayList<String>();
		StringBuilder current = new StringBuilder();
		boolean lastTokenHasBeenQuoted = false;
		
		while(tok.hasMoreTokens())
		{
			final String nextTok = tok.nextToken();
			switch(state)
			{
				case inQuote:
					if("\'".equals(nextTok))
					{
						lastTokenHasBeenQuoted = true;
						state = normal;
					}
					else
					{
						current.append(nextTok);
					}
					break;
				case inDoubleQuote:
					if("\"".equals(nextTok))
					{
						lastTokenHasBeenQuoted = true;
						state = normal;
					}
					else
					{
						current.append(nextTok);
					}
					break;
				default:
					if("\'".equals(nextTok))
					{
						state = inQuote;
					}
					else if("\"".equals(nextTok))
					{
						state = inDoubleQuote;
					}
					else if(" ".equals(nextTok))
					{
						if(lastTokenHasBeenQuoted || current.length() != 0)
						{
							list.add(current.toString());
							current = new StringBuilder();
						}
					}
					else
					{
						current.append(nextTok);
					}
					lastTokenHasBeenQuoted = false;
					break;
			}
		}
		
		if(lastTokenHasBeenQuoted || current.length() != 0)
		{
			list.add(current.toString());
		}
		
		if(state == inQuote || state == inDoubleQuote)
		{
			throw new IllegalArgumentException("Unbalanced quotes in " + toProcess);
		}
		
		final String[] args = new String[list.size()];
		return list.toArray(args);
	}
}
