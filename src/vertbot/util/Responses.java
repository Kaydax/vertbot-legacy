package vertbot.util;

import vertbot.commands.Command;
import vertbot.commands.HelpCommand;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class Responses
{
	public static void join(GuildJoinEvent event)
	{
		TextChannel chan = Utils.getLogicalChannel(event.getGuild());
		
		if(chan != null && !event.getGuild().getId().equals("110373943822540800"))
		{
			EmbedBuilder ebuilder = new EmbedBuilder();
			ebuilder.setTitle("Hello I'm Vertbot");
			ebuilder.setDescription("If you would like to change the default prefix do `-settings prefix <prefix you want>` to set my prefix. "
					+ "\nIf you would like to see what commands I can do just use the help command. "
					+ "\n[Join my discord for any help](https://discord.gg/WPUU2dF)");
			ebuilder.setColor(ColorUtil.neutral());
			MessageEmbed embed = ebuilder.build();
			MessageBuilder mbuilder = new MessageBuilder();
			mbuilder.setEmbed(embed);
			Message message = mbuilder.build();
			chan.sendMessage(message).complete();
		}
	}
	
	public static void help(MessageReceivedEvent event, Command command)
	{
		respond(event, "```\n" + new HelpCommand().getHelp(event, command) + "```");
	}
	
	public static void channelNotFoundId(MessageReceivedEvent event, String id)
	{
		respond(event, "Sorry, I couldn't find a channel with the id '" + id + "':frowning:");
	}
	
	public static void userNotFound(MessageReceivedEvent event, String name)
	{
		respond(event, "Sorry, I couldn't find a user by the name '" + name + "':frowning:");
	}
	
	public static void noPermission(MessageReceivedEvent event, String perm)
	{
		if(perm.equals(Permissions.DEV))
		{
			notDev(event);
		}
		else if(perm.equals(Permissions.OWNER))
		{
			notOwner(event);
		}
		else if(perm.equals(Permissions.ADMIN))
		{
			notAdmin(event);
		}
		else
		{
			Responses.respond(event, "There's probably a permission error somewhere");
		}
	}
	
	private static void notDev(MessageReceivedEvent event)
	{
		respond(event, "This is only for my dev.");
	}
	
	private static void notOwner(MessageReceivedEvent event)
	{
		respond(event, "You're not the owner of '" + event.getGuild().getName() + "'...  I can't do that for you.");
	}
	
	private static void notAdmin(MessageReceivedEvent event)
	{
		respond(event, "You're not an admin of '" + event.getGuild().getName() + "'...  I can't do that for you.");
	}
	
	public static void noPrivate(MessageReceivedEvent event)
	{
		respond(event, "Sorry... this command can't be used in private channels...");
	}
	
	public static void noGuild(MessageReceivedEvent event)
	{
		respond(event, "Sorry... this command can't be used in guild channels.... :frowning:");
	}
	
	/** responds to the author of the event, clips the response to 2000 characters */
	public static void respond(MessageReceivedEvent event, String response)
	{
		String mention = event.getAuthor().getAsMention();
		response = Utils.clip(response, 2000 - mention.length() - 1);
		
		event.getChannel().sendMessage(mention + " " + response).complete();
	}
}
