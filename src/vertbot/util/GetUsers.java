package vertbot.util;

import java.util.List;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;

public class GetUsers
{
	public static int Users(Guild guild)
	{
		List<Member> users = guild.getMembers();
		int nonbot = 0;
		for(int i = 0; i < users.size(); i++)
		{
			Member user = users.get(i);
			if(!user.getUser().isBot())
			{
				nonbot++;
			}
		}
		return nonbot;
	}
	
	public static int Bots(Guild guild)
	{
		List<Member> users = guild.getMembers();
		int bot = 0;
		for(int i = 0; i < users.size(); i++)
		{
			Member user = users.get(i);
			if(user.getUser().isBot())
			{
				bot++;
			}
		}
		return bot;
	}
	
	public static int Total(Guild guild) 
	{
		return guild.getMembers().size();
	}
}
