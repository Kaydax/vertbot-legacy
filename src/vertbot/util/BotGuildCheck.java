package vertbot.util;

import net.dv8tion.jda.core.entities.Guild;

public class BotGuildCheck
{
	public static boolean Check(Guild guild)
	{
		Integer bots  = GetUsers.Bots(guild);
		Integer total = GetUsers.Total(guild);
		
		if((bots / total)*100 >= 80)
		{
			return true;
		} else {
			return false;
		}
	}
}
