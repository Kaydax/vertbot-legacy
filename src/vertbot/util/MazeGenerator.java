package vertbot.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class MazeGenerator
{
	static Random RANDOM = new Random();
	
	public static BufferedImage drawMaze(int[][] maze) throws IOException
	{
		int width = maze.length;
		int height = maze[0].length;
		
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				bi.setRGB(x, height - 1 - y, Color.HSBtoRGB(maze[x][y] / (float) 1000f, 0.25f, 1)); //Color.HSBtoRGB(maze[x][y] / 1000f, 1, 1));
			}
		}
		
		return bi;
	}
	
	public static BufferedImage drawMaze2(int[][] maze) throws IOException
	{
		int width = maze.length;
		int height = maze[0].length;
		int avgDim = (width + height) / 2;
		
		BufferedImage bi = new BufferedImage(width * 2 + 1, height * 2 + 1, BufferedImage.TYPE_INT_ARGB);
		
		for(int y = 0; y < height; y++)
		{
			int ny = height - 1 - y; //from the top
			
			for(int x = 0; x < width; x++)
			{
			    int color = maze[x][y] == -1 ? 0xff000000 : Color.HSBtoRGB(maze[x][y] / (float) avgDim, 0.25f, 1);
				//int color = maze[x][y] == -1 ? 0xff000000 : 0xffffffff;
				
				bi.setRGB(x * 2, ny * 2, 0xff000000); //corner
				bi.setRGB(x * 2 + 1, ny * 2, isValidMove(maze, x, y, 1) ? color : 0xff000000); //north wall
				bi.setRGB(x * 2, ny * 2 + 1, isValidMove(maze, x, y, 2) ? color : 0xff000000); //west wall
				bi.setRGB(x * 2 + 1, ny * 2 + 1, color); //cell
			}
			
			bi.setRGB(width * 2, ny * 2, 0xff000000); //"corner" wall
			bi.setRGB(width * 2, ny * 2 + 1, 0xff000000); //"cell" wall
		}
		
		for(int x = 0; x < width; x++)
		{
			bi.setRGB(x * 2, height * 2, 0xff000000); //"corner" wall
			bi.setRGB(x * 2 + 1, height * 2, 0xff000000); //"cell" wall
		}
		bi.setRGB(width * 2, height * 2, 0xff000000); //last corner wall
		
		return bi;
	}
	
	public static boolean isValidMove(int[][] maze, int x, int y, int dir)
	{
		int[] dxy = dir2xy(dir);
		
		if(!isInRange(maze, x, y) || !isInRange(maze, x + dxy[0], y + dxy[1]))
		{
			return false;
		}
		
		int a = maze[x][y];
		int b = maze[x + dxy[0]][y + dxy[1]];
		
		return Math.abs(a - b) == 1;
	}
	
	public static int[][] generate(int width, int height)
	{
		int[][] maze = new int[width][height];
		initMaze(maze);
		
		ArrayDeque<int[]> moves = new ArrayDeque<int[]>();
		moves.push(new int[]
		{ width / 2, height / 2 });
		
		while(moves.size() > 0)
		{
			int[] xy = moves.peek();
			
			maze[xy[0]][xy[1]] = moves.size() - 1;
			
			Integer[] dirs =
			{ 0, 1, 2, 3 };
			
			Collections.shuffle(Arrays.asList(dirs));
			
			boolean dirFound = false;
			
			for(int i = 0; i < dirs.length; i++)
			{
				int[] dxy = dir2xy(dirs[i]);
				
				if(isInRange(maze, xy[0] + dxy[0], xy[1] + dxy[1]) && maze[xy[0] + dxy[0]][xy[1] + dxy[1]] == -1)
				{
					moves.push(new int[]
					{ xy[0] + dxy[0], xy[1] + dxy[1] });
					dirFound = true;
					
					break;
				}
			}
			
			if(!dirFound)
			{
				moves.pop();
			}
		}
		
		return maze;
	}
	
	public static int[][] generate2(int width, int height)
	{
		int[][] maze = new int[width][height];
		initMaze(maze);
		
		ArrayDeque<int[]> moves = new ArrayDeque<int[]>();
		moves.push(new int[]
		{ RANDOM.nextInt(width), RANDOM.nextInt(height) });
		
		while(moves.size() > 0)
		{
			int[] xy = moves.peek();
			
			maze[xy[0]][xy[1]] = moves.size() - 1;
			
			Integer[] dirs =
			{ 0, 1, 2, 3 };
			
			Collections.shuffle(Arrays.asList(dirs));
			
			boolean dirFound = false;
			
			for(int i = 0; i < dirs.length; i++)
			{
				int[] dxy = dir2xy(dirs[i]);
				
				if(isInRange(maze, xy[0] + dxy[0], xy[1] + dxy[1]) && maze[xy[0] + dxy[0]][xy[1] + dxy[1]] == -1)
				{
					moves.push(new int[]
					{ xy[0] + dxy[0], xy[1] + dxy[1] });
					dirFound = true;
					
					break;
				}
			}
			
			if(!dirFound)
			{
				moves.pop();
			}
		}
		
		return maze;
	}
	
	public static void initMaze(int[][] maze)
	{
		for(int i = 0; i < maze.length; i++)
		{
			for(int j = 0; j < maze[i].length; j++)
			{
				maze[i][j] = -1;
			}
		}
	}
	
	public static int[] dir2xy(int dir)
	{
		dir %= 4;
		
		switch(dir)
		{
			case 0:
				return new int[]
				{ 1, 0 };
			case 1:
				return new int[]
				{ 0, 1 };
			case 2:
				return new int[]
				{ -1, 0 };
			case 3:
				return new int[]
				{ 0, -1 };
		}
		
		//should never happen
		return new int[]
		{ 0, 0 };
	}
	
	public static boolean isInRange(int[][] maze, int x, int y)
	{
		return x >= 0 && x < maze.length && y >= 0 && y < maze[x].length;
	}
	
}
