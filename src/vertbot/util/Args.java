package vertbot.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Args
{
	private String[] args;
	
	//TODO: first, rest
	
	public Args(String source)
	{
		List<String> list = new ArrayList<String>();
		
		/*
		Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(source);
		
		while(m.find())
		{
			list.add(m.group(1)); //replace quotes on get
		}
		*/
		
		//Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
		Pattern regex = Pattern.compile("\"([^\"]*)\"|'([^']*)'|[^\\s]+");
		
		Matcher regexMatcher = regex.matcher(source);
		while(regexMatcher.find())
		{
			if(regexMatcher.group(1) != null)
			{
				// Add double-quoted string without the quotes
				list.add("\"" + regexMatcher.group(1) + "\"");
			}
			else if(regexMatcher.group(2) != null)
			{
				// Add single-quoted string without the quotes
				list.add("'" + regexMatcher.group(2) + "'");
			}
			else
			{
				// Add unquoted word
				list.add(regexMatcher.group());
			}
		}
		
		this.args = list.toArray(new String[0]);
	}
	
	/** like a substring, except with args */
	private Args(Args source, int start, int end)
	{
		if(end < 0)
		{
			end = source.size();
		}
		
		if(end <= start)
		{
			this.args = new String[0];
			
			return;
		}
		
		int size = end - start;
		
		this.args = new String[size];
		
		for(int i = 0; i < size; i++)
		{
			this.args[i] = source.get(start + i);
		}
	}
	
	/** like a substring, except with args */
	public Args(String source, int start, int end)
	{
		//lol
		this.setArgs(new Args(new Args(source), start, end).getArgs());
	}
	
	public String concat()
	{
		return this.concat(0);
	}
	
	public String concat(int start)
	{
		return this.concat(start, this.size());
	}
	
	public String concat(int start, int end)
	{
		return this.concat(start, end, true);
	}
	
	/** start, end positions, true to remove quotes */
	public String concat(int start, int end, boolean removeQuotes)
	{
		end = end == -1 ? this.size() : end;
		
		String ret = "";
		
		for(int i = start; i < end; i++)
		{
			ret += this.get(i, removeQuotes) + (i < end - 1 ? " " : "");
		}
		
		return ret;
	}
	
	public String get(int pos)
	{
		return this.get(pos, true);
	}
	
	/** returns null if out of bounds */
	public String get(int pos, boolean removeQuotes)
	{
		if(pos < 0 || pos >= this.size())
		{
			return null;
		}
		
		String ret = this.args[pos];
		
		if(removeQuotes)
		{
			
			//ret = ret.substring(start, end);//ret.replace("\"", "");
		}
		
		return ret;
	}
	
	public int size()
	{
		return this.args.length;
	}
	
	public void setArgs(String[] args)
	{
		this.args = args;
	}
	
	public String[] getArgs()
	{
		return this.args;
	}
	
	public String first()
	{
		return this.get(0);
	}
	
	public String rest()
	{
		return this.concat(1, -1, false);
	}
}
