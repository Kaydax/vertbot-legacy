package vertbot.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.dv8tion.jda.core.entities.Guild;

public class GuildSorter
{
	/** returns copy... */
	public static List<Guild> sortByUsers(List<Guild> guilds)
	{
		List<Guild> sortedGuilds = new ArrayList<>(guilds);
		
		//Collections.copy(sortedGuilds, guilds);
		
		Collections.sort(sortedGuilds, new UserComparator());
		
		return sortedGuilds;
	}
	
	public static class UserComparator implements Comparator<Guild>
	{
		@Override
		public int compare(Guild a, Guild b)
		{
			int aUsers = a.getMembers().size();
			int bUsers = b.getMembers().size();
			
			return aUsers > bUsers ? -1 : aUsers < bUsers ? 1 : 0;
		}
	}
	
}
