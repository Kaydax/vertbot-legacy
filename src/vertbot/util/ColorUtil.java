package vertbot.util;

import java.awt.Color;
import java.util.Random;

public class ColorUtil
{
	public static Color randomColor() {
		Random rand = new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		Color randomColor = new Color(r, g, b);
		return randomColor;
	}
	
	public static Color success() {
		Color success = new Color(0, 155, 25);
		return success;
	}
	
	public static Color error() {
		Color error = new Color(192, 64, 64);
		return error;
	}
	
	public static Color neutral() {
		Color neutral = new Color(0, 196, 177);
		return neutral;
	}
}
