package vertbot.settings;

public class GuildSettings extends Settings
{
	public static final String GUILD_PREFIX = "guild";
	String id;
	
	public GuildSettings(String id)
	{
		super();
		
		this.id = id;
	}
	
	@Override
	public String getPath()
	{
		return Settings.SETTINGS_PATH + "/" + GUILD_PREFIX + "_" + this.id + "." + Settings.SETTINGS_EXT;
	}
	
}
