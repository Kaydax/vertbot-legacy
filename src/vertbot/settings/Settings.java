package vertbot.settings;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class Settings
{
	public static final String SETTINGS_PATH = "settings";
	public static final String SETTINGS_EXT = "json";
	JSONObject json;
	
	public Settings()
	{
		this.json = new JSONObject();
	}
	
	public JSONObject getJSON()
	{
		return this.json;
	}
	
	/** clears settings and saves */
	public void reset()
	{
		this.json = new JSONObject();
		this.save();
	}
	
	public Object get(String setting)
	{
		//only works with . for now...
		String[] path = setting.split("\\.");
		
		JSONObject obj = this.json;
		
		//walk the tree
		for(int i = 0; i < path.length; i++)
		{
			String key = path[i];
			JSONObject opt = obj.optJSONObject(key);
			
			if(opt == null)
			{
				return null;
			}
			
			obj = opt;
		}
		
		return obj;
	}
	
	/** only works with "."... this could probably wipe path settings if youre not careful*/
	public boolean set(String setting, Object value)
	{
		//only works with . for now...
		String[] path = setting.split("\\.");
		
		JSONObject obj = this.json;
		
		//walk the tree
		for(int i = 0; i < path.length - 1; i++)
		{
			String key = path[i];
			JSONObject opt = obj.optJSONObject(key);
			
			//create object if its null (TODO: COULD OVERWRITE OLD DATA!!)
			if(opt == null)
			{
				obj.put(key, new JSONObject());
				opt = obj.getJSONObject(key);
			}
			
			obj = opt;
		}
		
		obj.put(path[path.length - 1], value);
		
		return true;
	}
	
	public void save()
	{
		try
		{
			Files.write(Paths.get(this.getPath()), this.json.toString(2).getBytes());
		}
		catch(JSONException | IOException ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void load()
	{
		try
		{
			this.json = new JSONObject(new String(Files.readAllBytes(Paths.get(this.getPath()))));
		}
		catch(JSONException ex)
		{
			ex.printStackTrace();
		}
		catch(IOException ex)
		{
			this.save();
			System.out.println("Created settings file '" + this.getPath() + "'");
		}
	}
	
	public abstract String getPath();
}
