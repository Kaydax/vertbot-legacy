var request = require("request");
var cheerio = require("cheerio");

var url = process.argv[2];

var request = request.defaults({
headers: { "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0" },
})

request(url, function (error, response, html) {
if (!error && response.statusCode == 200) {
var $ = cheerio.load(html,{ normalizeWhitespace: true, decodeEntities: true });
var title = $("title").text();
console.log(title);
}
else {
console.log("ERR: %j\t%j",error,response.statusCode);
}
});