var request = require('request');
var ps = require('xml2js').parseString;

const defaultOptions =
{
  "timeout": 3 * 1000,
  gzip: true
};

var limit = 100;

var url = "http://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=" + limit + "&tags=" + process.argv[2];

request.defaults(defaultOptions)({uri: url}, function(err, resp, body)
{
	try 
	{
		if (!err && resp.statusCode == 200)
		{
			ps(body, function(err, data)
			{
				var posts = data.posts.post;
				var out = {};
				out.url = posts[Math.floor(Math.random() * posts.length)].$.file_url;
				out.tags = posts[Math.floor(Math.random() * posts.length)].$.tags;

				console.log(JSON.stringify(out));
			});
		}
	} catch(err) {
		console.log("No results found for the tag(s):`" + process.argv[2] + "`");
	}
});
