var urban = require('urban'),
    input = urban(process.argv[2]);

input.first(function(json) 
{
	try
	{
		console.log(json.definition + "\n\n" + json.example + "\n\n - Definition By: " + json.author + " -");		
	} catch(err) {
		console.log("Nothing found, sorry.");
	}
});