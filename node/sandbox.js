const vm = require('vm');
var fs = require("fs");

var sandbox = {};

try
{
  sandbox = JSON.parse(fs.readFileSync("sandbox.json", {encoding:"utf8"}));
}
catch(e)
{

}

sandbox.console = console;

var ctx = vm.createContext(sandbox);

console.log(vm.runInContext(process.argv[2], ctx, {timeout:1000}));

fs.writeFileSync("sandbox.json", JSON.stringify(sandbox), {encoding:"utf8"});
