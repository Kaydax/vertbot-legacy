var YouTube = require('youtube-node');

var youTube = new YouTube();

youTube.setKey('{youtube-api-Token}');

youTube.search(process.argv[2], 5, function(error, result) {
  if (error) {
    console.log(error);
  }
  else {
    console.log(JSON.stringify(result, null, 5));
  }
});
