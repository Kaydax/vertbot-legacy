var request = require('request');
var ps = require('xml2js').parseString;
var cheerio = require('cheerio');
var fs = require('fs');

const defaultOptions =
{
  "timeout": 3 * 1000,
  gzip: true
};

request('https://xkcd.com/archive/', function(err, resp, body){
  $ = cheerio.load(body);
  links = $('a'); //use your CSS selector here
  var edited = "{";
  $(links).each(function(i, link){
    edited += '"' + $(link).text() + '":"' + $(link).attr('href') + '",\n';
  });
  edited = edited.slice(0, -988);
  edited += "}";
  fs.writeFile("./xkcd/cache.json", edited, function(err) {
      if(err) {
          return console.log(err);
      }

      console.log("The file was saved!");
  });

});
